package kr.swmaestro.sddp.iwillpass;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.BuildConfig;
import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.Error;
import net.androidsea.seoultaxi.base.api.HttpJsonAPI;
import net.androidsea.seoultaxi.base.image.ImageDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import kr.swmaestro.sddp.iwillpass.model.SimulationExamData;
import kr.swmaestro.sddp.iwillpass.model.SimulationUserAnswerData;
import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;
import kr.swmaestro.sddp.iwillpass.view.ResizableImageView;

/**
 * Created by rino0601 on 2013. 11. 3..
 */
public class SimulationExamActivity extends GoogleAnalyticsActivity implements RadioGroup.OnCheckedChangeListener {
    public static final String subjectKey = "subjectKey";
    private String subject;

    private Context context;
    private ResizableImageView imageView;
    private ProgressBar progressBarNormal;
    private RadioGroup radioGroup;
    private RuleExplainDialog ruleExplainDialog;
    private List<SimulationExamData> problemList;

    private long currentStartTime;
    private int currentProblemNumber;
    private String checkedValue;

    private int totalTime;
    private int totalPoint;


    /**
     * Intent Extars 에서 값을 불러온다.
     */
    private void injectExtras() {
        Intent intent_ = getIntent();
        Bundle extras_ = intent_.getExtras();
        if (extras_ != null) {
            if (extras_.containsKey(subjectKey)) {
                this.subject = (String) extras_.get(subjectKey);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_exam);
        this.context = this;
        imageView = (ResizableImageView) findViewById(R.id.imageView);
        progressBarNormal = (ProgressBar) findViewById(R.id.progressBarNormal);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(this);

        Button nextButton = (Button) findViewById(R.id.buttonNext);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storeCurrentUserAnswer();
                presentNextProblem();
            }
        });

        initNetwork();


        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user", kr.swmaestro.sddp.iwillpass.util.SystemInfo.getHashedPhoneNumber(context));
            jsonObject.put("subject", subject);
            HttpJsonAPI.request("/api/v1.0/exam/today", "GET", jsonObject, new HttpJsonAPI.CallbackListener() {
                @Override
                public void callback(int error, JSONObject result) {
                    if (error != Error.SUCCESS) {
                        sendException(new Exception("시험지 다운로드 실패"));
                        Toast.makeText(context, "데이터 다운로드에 실패 했습니다", Toast.LENGTH_SHORT).show();
                        finish();
                        return;
                    }
                    if (BuildConfig.DEBUG)
                        Toast.makeText(context, "일단 동작!", Toast.LENGTH_SHORT).show();
                    try {
                        JSONArray dataList = result.getJSONArray("data");
                        int dataCount = result.getInt("data_count");
                        int totalPoint = result.getInt("total_point");
                        int totalTime = result.getInt("total_time");

                        List<SimulationExamData> problemArrayListBuffer = new ArrayList<SimulationExamData>();
                        for (int i = 0; i < dataCount; i++) {
                            JSONObject data = dataList.getJSONObject(i);
                            String id = data.getString("id");
                            String answer = data.getString("answer");
                            String questionImageUrl = data.getString("question_image_url");
                            int point = data.getInt("point");
                            String type = data.getString("type");
                            problemArrayListBuffer.add(new SimulationExamData(id, answer, questionImageUrl, point, type));
                        }

                        settingActivity(totalTime, totalPoint, problemArrayListBuffer);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        sendException(e);
                        Toast.makeText(context, "데이터 다운로드에 실패 했습니다", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });
        } catch (JSONException e) {
            sendException(e);
        }
        ruleExplainDialog = new RuleExplainDialog(this);
        ruleExplainDialog.show();
    }

    private void storeCurrentUserAnswer() {
        long currentEndTime = System.currentTimeMillis();
        SimulationExamData simulationExamData = problemList.get(currentProblemNumber);
        String id = simulationExamData.getId();
        SimulationUserAnswerData simulationUserAnswerData = new SimulationUserAnswerData(id, checkedValue, currentEndTime - currentStartTime);
        SimulationUserAnswerData.save(this, simulationUserAnswerData);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        // prevent back key.
    }


    private void settingActivity(int totalTime, int totalPoint, List<SimulationExamData> problemArrayList) {
        this.totalTime = totalTime;
        this.totalPoint = totalPoint;
        this.problemList = problemArrayList;
        this.currentProblemNumber = 0;

        progressBarNormal.setMax(totalTime);
        progressBarNormal.setProgress(totalTime);

        presentProblem(currentProblemNumber);

        ruleExplainDialog.activityIsReady();
    }

    private void closeActivity() {
        setResult(RESULT_OK, getIntent());
        finish();
    }

    private void startExam() {
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                int progress = progressBarNormal.getProgress();
                if (progress == 0) {
                    t.cancel();
                }
                progressBarNormal.setProgress(progress - 1000);
            }
        }, 0, 1000);
    }

    private void presentProblem(int problemNumber) {
        SimulationExamData problem = problemList.get(problemNumber);
        // fixme when 0, it occur throwIndexOutOfBoundsException 에 대한 처리.

        ImageDownloader imageDownloader = new ImageDownloader(this, imageView);
        imageDownloader.execute(problem.getQuestionImageUrl());
        radioGroup.clearCheck();
    }

    private void presentNextProblem() {
        this.currentProblemNumber += 1;
        if (currentProblemNumber == problemList.size()) {
            // (API) send result to Server
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user", kr.swmaestro.sddp.iwillpass.util.SystemInfo.getHashedPhoneNumber(context));
                putUserAnswersAndPoint(jsonObject);
                jsonObject.put("total_point", totalPoint);
                jsonObject.put("subject", subject);
                HttpJsonAPI.request("/api/v1.0/exam/today", "POST", jsonObject, new HttpJsonAPI.CallbackListener() {
                    @Override
                    public void callback(int error, JSONObject result) {
                        if (error != Error.SUCCESS) {
                            sendException(new Exception("POST Fail"));
                        }
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ScoringDialog scoringDialog = new ScoringDialog(this);
            scoringDialog.show();
        } else {
            presentProblem(currentProblemNumber);
        }
    }

    private void putUserAnswersAndPoint(JSONObject jsonObject) throws JSONException {
        int total = 0;
        JSONArray result = new JSONArray();
        for (SimulationExamData aProblem : problemList) {
            SimulationUserAnswerData aAnswer = SimulationUserAnswerData.load(this, aProblem.getId());

            JSONObject jsonA = new JSONObject();
            jsonA.put("id", aAnswer.getId());
            jsonA.put("answer", aAnswer.getAnswer());
            jsonA.put("time", aAnswer.getTime());

            String user = aAnswer.getAnswer();
            String problem = aProblem.getAnswer();
            if (user.equals(problem)) {
                total += aProblem.getPoint();
                jsonA.put("correct_answer", true);
            } else {
                jsonA.put("correct_answer", false);
            }

            result.put(jsonA);
        }
        jsonObject.put("answers", result);
        jsonObject.put("user_point", total);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case -1:
                checkedValue = "";
                break;
            case R.id.radioButton1:
                checkedValue = "1";
                break;
            case R.id.radioButton2:
                checkedValue = "2";
                break;
            case R.id.radioButton3:
                checkedValue = "3";
                break;
            case R.id.radioButton4:
                checkedValue = "4";
                break;
            case R.id.radioButton5:
                checkedValue = "5";
                break;
        }
    }

    private class RuleExplainDialog extends Dialog {

        private final Button nextButton;

        private RuleExplainDialog(Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_rule_explain);
            nextButton = (Button) findViewById(R.id.button);
            nextButton.setText("서버에서 문제를 받아오는 중입니다.");
            nextButton.setEnabled(false);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startExam();
                    dismiss();
                    currentStartTime = System.currentTimeMillis();
                }
            });
        }

        @Override
        public void onBackPressed() {
            // prevent back key;
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            Rect dialogBounds = new Rect();
            getWindow().getDecorView().getHitRect(dialogBounds);
            if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
                return false; // prevent outside touch dismiss
            }
            return super.dispatchTouchEvent(ev);
        }

        public void activityIsReady() {
            nextButton.setText("시작하기");
            nextButton.setEnabled(true);
        }

    }

    private class ScoringDialog extends Dialog {
        private ScoringDialog(Context context) {
            super(context);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_rule_explain);

            TextView textViewTitle = (TextView) findViewById(R.id.textViewTitle);
            textViewTitle.setText("== 채점결과(" + subject + ") ==");

            TextView textViewContent = (TextView) findViewById(R.id.textViewContent);
            textViewContent.setText(getExamResultString());

            Button button = (Button) findViewById(R.id.button);
            button.setText("닫기");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeActivity();
                }
            });
        }

        private String getExamResultString() {
            String result = "";
            int total = 0;
            int problemNumber = 1;
            String sFormat = "%d번 (%d점) >\n정답 %s 내 답 %s = %s\n";
            for (SimulationExamData aProblem : problemList) {
                SimulationUserAnswerData aAnswer = SimulationUserAnswerData.load(context, aProblem.getId());

                int point = aProblem.getPoint();
                String problem = aProblem.getAnswer();
                String userAnswer = aAnswer.getAnswer();
                boolean isCorrect;

                if (userAnswer.equals(problem)) {
                    total += point;
                    isCorrect = true;
                } else {
                    isCorrect = false;
                }
                String formatString = String.format(sFormat, problemNumber, point, problem, userAnswer, (isCorrect ? "맞음" : "틀림"));
                result += formatString;
                problemNumber += 1;
            }

            String time = (totalTime - progressBarNormal.getProgress()) / 1000 + "초";
            result += "\n" +
                    "총 합 : " + total + "점 (만점 " + totalPoint + "점)\n" +
                    "과목 소요 시간  : " + time + "\n" +
                    "내일, 각 문제 오답률을 볼 수 있어요!";
            return result;
        }
    }


}

