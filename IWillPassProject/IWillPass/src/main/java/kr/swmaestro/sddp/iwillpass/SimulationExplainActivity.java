package kr.swmaestro.sddp.iwillpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.BuildConfig;
import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.image.ImageDownloader;

import java.util.List;

import kr.swmaestro.sddp.iwillpass.model.SimulationExplainData;
import kr.swmaestro.sddp.iwillpass.model.SimulationUserAnswerData;
import kr.swmaestro.sddp.iwillpass.view.ResizableImageView;

/**
 * 이 액티비티에 진입했을땐, 이미 데이터는 다 로드 되어 있다고 생각하고, 인텐트에서 받아 올것.
 * Created by rino0601 on 2013. 11. 11..
 */
public class SimulationExplainActivity extends Activity {
    public static final String dataArrayKey = "dataArrayKey";
    private List<SimulationExplainData> dataList;

    private ResizableImageView imageViewQuestion;
    private TextView textViewAnswerCmp;
    private TextView textViewErrorRate;
    private ResizableImageView imageViewExplain;
    private ImageButton[] ibtnArray;

    private int position;


    /**
     * Intent Extars 에서 값을 불러온다.
     */
    private void injectExtras() {
        Intent intent_ = getIntent();
        Bundle extras_ = intent_.getExtras();
        if (extras_ != null) {
            if (extras_.containsKey(dataArrayKey)) {
                this.dataList = (List<SimulationExplainData>) extras_.get(dataArrayKey);
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        injectExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_explain);

        //뷰와 리스너 연결.
        imageViewQuestion = (ResizableImageView) findViewById(R.id.imageViewQuestion);
        textViewAnswerCmp = (TextView) findViewById(R.id.textViewAnswerCmp);
        textViewErrorRate = (TextView) findViewById(R.id.textViewErrorRate);
        imageViewExplain = (ResizableImageView) findViewById(R.id.imageViewExplain);

        ibtnArray = new ImageButton[5];
        ibtnArray[0] = (ImageButton) findViewById(R.id.ibtn1);
        ibtnArray[0].setOnClickListener(positionButtonListener);
        ibtnArray[1] = (ImageButton) findViewById(R.id.ibtn2);
        ibtnArray[1].setOnClickListener(positionButtonListener);
        ibtnArray[2] = (ImageButton) findViewById(R.id.ibtn3);
        ibtnArray[2].setOnClickListener(positionButtonListener);
        ibtnArray[3] = (ImageButton) findViewById(R.id.ibtn4);
        ibtnArray[3].setOnClickListener(positionButtonListener);
        ibtnArray[4] = (ImageButton) findViewById(R.id.ibtn5);
        ibtnArray[4].setOnClickListener(positionButtonListener);

        //첫번째 아이템을 보여줌
        presentItem(0);
    }

    private void presentItem(int position) {
        // 유효성 검사
        if (position < 0 || position >= ibtnArray.length)
            return;

        // 현재 위치 설정.
        this.position = position;

        // 원하는 아이템 가져오기
        SimulationExplainData data = dataList.get(position);

        // 뷰에 반영하기
        ImageDownloader questImageDownloader = new ImageDownloader(this, imageViewQuestion);
        questImageDownloader.execute(data.getQuestionImageUrl());
        ImageDownloader expalinImageDownloader = new ImageDownloader(this, imageViewExplain);
        expalinImageDownloader.execute(data.getExplainImageUrl());

        textViewAnswerCmp.setText(String.format("정답 :%s / 내 답 :%s (%d점)", data.getAnswer(), SimulationUserAnswerData.load(this, data.getId()).getAnswer(), data.getPoint()));
        textViewErrorRate.setText(String.format("%3d%%", data.getErrorRate()));

        if (BuildConfig.DEBUG)
            Toast.makeText(this, "presentItem:" + position, Toast.LENGTH_SHORT).show();
    }

    private View.OnClickListener positionButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageButton ibtn = (ImageButton) v;
            ibtnArray[position].setBackgroundResource(R.drawable.community_unpoint_list);
            ibtn.setBackgroundResource(R.drawable.community_select_list);
            for (int i = 0; i < ibtnArray.length; i++) {
                if (ibtnArray[i] == v) {
                    presentItem(i);
                }
            }
        }
    };
}