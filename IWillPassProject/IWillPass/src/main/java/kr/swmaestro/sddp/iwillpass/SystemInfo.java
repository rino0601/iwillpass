package kr.swmaestro.sddp.iwillpass;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Base64;

import net.androidsea.seoultaxi.base.util.GMLog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by rino0601 on 13. 10. 9..
 */
public class SystemInfo {
    public static String getPhoneNumber(Context context) {
        if (context == null)
            return null;

        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String num = telManager.getLine1Number();

        if (num == null) {
            num = "01000000000";
        }

        GMLog.d(SystemInfo.class.getName(), "getPhoneNumber " + num);
        return num;
    }

    public static String getHashedPhoneNumber(Context context) {
        if (context == null)
            return null;

        String line1Num = getPhoneNumber(context);

        MessageDigest md_md5 = null;
        try {
            md_md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

        md_md5.update(line1Num.getBytes());
        byte[] rawNum = md_md5.digest();
        String udid = Base64.encodeToString(rawNum, Base64.NO_PADDING | Base64.NO_WRAP);
        return udid;
    }

}
