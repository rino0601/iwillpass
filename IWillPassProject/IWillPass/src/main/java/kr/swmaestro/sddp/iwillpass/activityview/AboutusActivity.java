package kr.swmaestro.sddp.iwillpass.activityview;

import android.os.Bundle;

import com.dulgi.MoGye.dev.R;

import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

/**
 * Created by FlaShilver on 2013. 10. 20..
 */
public class AboutusActivity extends GoogleAnalyticsActivity {

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.about_us);
    }
}
