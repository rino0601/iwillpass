package kr.swmaestro.sddp.iwillpass.activityview;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

/**
 * Created by Jahehwacho on 2013. 10. 20..
 */
public class CalculatorActivity extends GoogleAnalyticsActivity {

    private int etc_subject_id = 0;
    private int[] subject_origin_point = {100, 100, 100, 50, 50}; //국,수,영,사탐,과탐
    private int[] subject_origin_exact_count = {40, 35, 40, 20, 20}; //국,수,영,사탐,과탐

    private int[] subject_point = {100, 100, 100, 50, 50}; //국,수,영,사탐,과탐
    private int[] exact_count = {40, 35, 40, 20, 20}; //국,수,영,사탐,과탐
    private int[] wrong_count = {0, 0, 0, 0, 0}; //국,수,영,사탐,과탐

    final int SUBJECT_KOREAN = 0;
    final int SUBJECT_MATH = 1;
    final int SUBJECT_ENGLISH = 2;
    final int SUBJECT_ETC1 = 3;
    final int SUBJECT_ETC2 = 4;

    private int current_subject = SUBJECT_KOREAN;

    private void setSujectImageClear() {
        ImageView item = (ImageView) findViewById(R.id.calculator_activity_subject_korean);
        item.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb01kr_off));

        item = (ImageView) findViewById(R.id.calculator_activity_subject_english);
        item.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb03en_off));

        item = (ImageView) findViewById(R.id.calculator_activity_subject_math);
        item.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb02mt_off));

        item = (ImageView) findViewById(R.id.calculator_activity_subject_etc);
        if (etc_subject_id == 0)
            item.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb04tm1_off));
        else
            item.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb04tm2_off));
    }


    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_calculator);

        //InitButton
        TextView pointView = (TextView) findViewById(R.id.calculator_activity_subject_point);
        pointView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initPoint(current_subject);
            }
        });

        //Font 설정
        final Typeface robotoregular = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        pointView.setTypeface(robotoregular);

        //과목선택
        final ImageView[] subject_list = {
                (ImageView) findViewById(R.id.calculator_activity_subject_korean),
                (ImageView) findViewById(R.id.calculator_activity_subject_english),
                (ImageView) findViewById(R.id.calculator_activity_subject_math),
                (ImageView) findViewById(R.id.calculator_activity_subject_etc)};
        View.OnClickListener subject_listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSujectImageClear();
                ImageView view = (ImageView) v;
                Toast.makeText(CalculatorActivity.this, "점수를 누르면 초기화됩니다!", Toast.LENGTH_SHORT).show();
                switch (v.getId()) {
                    case R.id.calculator_activity_subject_korean:
                        //국어 : 45문항 (2점-35개, 3점-10개), 80분, 100점
                        current_subject = SUBJECT_KOREAN;
                        view.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb01kr_on));
                        break;
                    case R.id.calculator_activity_subject_english:
                        //영어 : 45문항 (2점-35개, 3점-10개), 70분, 100점
                        current_subject = SUBJECT_ENGLISH;
                        view.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb03en_on));
                        break;
                    case R.id.calculator_activity_subject_math:
                        //수학 : 30문항 (2점-3개, 3점-14개, 4점-13개), 100분, 100점
                        current_subject = SUBJECT_MATH;
                        view.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb02mt_on));
                        break;
                    default:
                        //사탐 : 20문항 (2점-10개, 3점-10개), 30분, 50점
                        //과탐 : 20문항 (2점-10개, 3점-10개), 30분, 50점
                        if (etc_subject_id == 0) {
                            current_subject = SUBJECT_ETC1;
                            view.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb04tm1_on));
                        } else {
                            current_subject = SUBJECT_ETC2;
                            view.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb04tm2_on));
                        }
                        break;
                }
                setPointAndButton(current_subject);
            }
        };

        for (int i = 0; i < subject_list.length; i++) {
            ImageView item = subject_list[i];
            item.setOnClickListener(subject_listener);
        }

        //탐1<->탐2
        View.OnClickListener etcSubejctChageListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSujectImageClear();
                ImageView subject = (ImageView) findViewById(R.id.calculator_activity_subject_etc);
                etc_subject_id = (etc_subject_id + 1) % 2;
                if (etc_subject_id == 0) {
                    current_subject = SUBJECT_ETC1;
                    subject.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb04tm1_on));
                } else {
                    current_subject = SUBJECT_ETC2;
                    subject.setImageDrawable(getResources().getDrawable(R.drawable.calculation_sb04tm2_on));
                }
                setPointAndButton(current_subject);
            }
        };
        ImageView item = (ImageView) findViewById(R.id.calculator_activity_subject_up);
        item.setOnClickListener(etcSubejctChageListener);

        item = (ImageView) findViewById(R.id.calculator_activity_subject_down);
        item.setOnClickListener(etcSubejctChageListener);

        //채점기능
        View.OnClickListener pointButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int point = 2;
                switch (view.getId()) {
                    case R.id.calculator_activity_point2_button:
                        break;
                    case R.id.calculator_activity_point3_button:
                        point = 3;
                        break;
                    default:
                        point = 4;
                        break;
                }
                if (subject_point[current_subject] >= point) {
                    subject_point[current_subject] -= point;
                    wrong_count[current_subject] += 1;
                    exact_count[current_subject] -= 1;
                    setPointAndButton(current_subject);
                } else {
                    Toast.makeText(CalculatorActivity.this, "0점 이하의 점수는 없습니다!", Toast.LENGTH_LONG).show();
                }
            }
        };

        ImageButton button = (ImageButton) findViewById(R.id.calculator_activity_point2_button);
        button.setOnClickListener(pointButtonListener);

        button = (ImageButton) findViewById(R.id.calculator_activity_point3_button);
        button.setOnClickListener(pointButtonListener);

        button = (ImageButton) findViewById(R.id.calculator_activity_point4_button);
        button.setOnClickListener(pointButtonListener);
    }

    private void setPointAndButton(int subject_id) {
        TextView subjectPoint = (TextView) findViewById(R.id.calculator_activity_subject_point);
        TextView exactCount = (TextView) findViewById(R.id.calculator_activity_exact_count);
        TextView wrongCount = (TextView) findViewById(R.id.calculator_activity_wrong_count);
        TextView totalPoint = (TextView) findViewById(R.id.calculator_activity_total_point);

        subjectPoint.setText(String.format("%d", subject_point[subject_id]));
        exactCount.setText(String.format("정답 %d개", exact_count[subject_id]));
        wrongCount.setText(String.format("오답 %d개", wrong_count[subject_id]));
        totalPoint.setText(String.format("총 %d문제", subject_origin_exact_count[subject_id]));

        //Button 조절 (수학에서만 4점짜리 존재)
        ImageButton button = (ImageButton) findViewById(R.id.calculator_activity_point4_button);
        if (subject_id == SUBJECT_MATH) {
            button.setVisibility(View.VISIBLE);
        } else {
            button.setVisibility(View.GONE);
        }
    }

    private void initPoint(int subject_id) {
        subject_point[subject_id] = subject_origin_point[subject_id];
        exact_count[subject_id] = subject_origin_exact_count[subject_id];
        wrong_count[subject_id] = 0;
        TextView subjectPoint = (TextView) findViewById(R.id.calculator_activity_subject_point);
        TextView exactCount = (TextView) findViewById(R.id.calculator_activity_exact_count);
        TextView wrongCount = (TextView) findViewById(R.id.calculator_activity_wrong_count);
        subjectPoint.setText(String.format("%d", subject_point[subject_id]));
        exactCount.setText(String.format("정답 %d개", exact_count[subject_id]));
        wrongCount.setText(String.format("오답 %d개", wrong_count[subject_id]));
    }
}
