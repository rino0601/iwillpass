package kr.swmaestro.sddp.iwillpass.activityview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

/**
 * Created by FlaShilver on 2013. 11. 5..
 */
public class CommunityActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        TextView toptitle = (TextView) findViewById(R.id.toptitle);
        ListView listview = (ListView) findViewById(R.id.community_list);
        Button pre_btn = (Button) findViewById(R.id.previous);
        Button next_btn = (Button) findViewById(R.id.next);

        ImageButton imgbtn1 = (ImageButton) findViewById(R.id.ibtn1);
        ImageButton imgbtn2 = (ImageButton) findViewById(R.id.ibtn2);
        ImageButton imgbtn3 = (ImageButton) findViewById(R.id.ibtn3);
        ImageButton imgbtn4 = (ImageButton) findViewById(R.id.ibtn4);
        ImageButton imgbtn5 = (ImageButton) findViewById(R.id.ibtn5);


        pre_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgbtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "1", Toast.LENGTH_SHORT).show();

            }
        });

        imgbtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "2", Toast.LENGTH_SHORT).show();

            }
        });

        imgbtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "3", Toast.LENGTH_SHORT).show();

            }
        });

        imgbtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "4", Toast.LENGTH_SHORT).show();

            }
        });

        imgbtn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "5", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
