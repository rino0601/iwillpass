package kr.swmaestro.sddp.iwillpass.activityview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dulgi.MoGye.dev.R;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

/**
 * Created by FlaShilver on 2013. 10. 15..
 */
public class DynamicTimerActivity extends GoogleAnalyticsActivity {

    Handler handler;
    long diffday, difftime, diffminutes, diffsecond, diffmilisecond;
    String testday = "20141106";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamictimer);
        final ImageButton modify = (ImageButton) findViewById(R.id.modify);
        final TextView dday = (TextView) findViewById(R.id.dday);
        final TextView timer = (TextView) findViewById(R.id.timer);
        final TextView mindcontrol = (TextView) findViewById(R.id.mindcontrol);
        final TextView mindcontrol_text = (TextView) findViewById(R.id.mindcontrol_text);
        final Typeface robotoregular = Typeface.createFromAsset(getAssets(),
                "fonts/Roboto-Medium.ttf");
        dday.setTypeface(robotoregular);

        final Typeface bareundotumotf2 = Typeface.createFromAsset(getAssets(),
                "fonts/bareundotumotf2.otf");
        //  year_indicator.setTypeface(bareundotumotf2);
        mindcontrol.setTypeface(bareundotumotf2);
        mindcontrol_text.setTypeface(bareundotumotf2);

        final Typeface bareundotumotf1 = Typeface.createFromAsset(getAssets(),
                "fonts/bareundotumotf1.otf");
        timer.setTypeface(bareundotumotf1);

        modify.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater.from(DynamicTimerActivity.this);
                final View layout = inflater.inflate(R.layout.custom_dialog, null);

                AlertDialog.Builder aDialog = new AlertDialog.Builder(DynamicTimerActivity.this);
                aDialog.setTitle("자신만의 다짐문구를 설정해주세요");
                aDialog.setView(layout);


                aDialog.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final EditText et = (EditText) layout.findViewById(R.id.et);
                        mindcontrol_text.setText(et.getText() + "");
                    }
                });
                aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog ad = aDialog.create();
                ad.show();

            }
        });


        handler = new Handler() {
            public void handleMessage(Message mgs) {
                Date today = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                Date finaltestday = formatter.parse(testday, new ParsePosition(
                        0));
                diffday = finaltestday.getTime() - today.getTime();
                long diffDays = diffday / (24 * 60 * 60 * 1000);
                difftime = (diffday / (60 * 60 * 1000) % 24);
                diffminutes = (diffday / (60 * 1000) % 60);
                diffsecond = (diffday / (1000) % 60);
                diffmilisecond = (diffday / (100) % 10);
                dday.setText("D-" + (diffDays + 1) + "");
                timer
                        .setText(diffDays + ":" + difftime + ":" + diffminutes
                                + ":" + diffsecond + ":" + diffmilisecond);
                handler.sendEmptyMessageDelayed(0, 100);
            }
        };

        handler.sendEmptyMessage(0);
    }


}
