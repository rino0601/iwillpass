package kr.swmaestro.sddp.iwillpass.activityview;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.HttpJsonAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import kr.swmaestro.sddp.iwillpass.SystemInfo;
import kr.swmaestro.sddp.iwillpass.model.Answer;
import kr.swmaestro.sddp.iwillpass.model.ExamInfo;
import kr.swmaestro.sddp.iwillpass.model.ExamResult;
import kr.swmaestro.sddp.iwillpass.model.FileStorage;
import kr.swmaestro.sddp.iwillpass.model.INTENT_KEY;
import kr.swmaestro.sddp.iwillpass.model.PapersInfo;
import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

/**
 * Created by FlaShilver on 2013. 10. 1..
 */
public class ExamInfoActivity extends GoogleAnalyticsActivity implements View.OnClickListener, HttpJsonAPI.CallbackListener {

    private PapersInfo papersInfo = null;
    private ExamInfo mExamInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examinfo);

        loadIntentExtras();

        TextView header = (TextView) findViewById(R.id.textView_untouchable_center);
        header.setText(mExamInfo.getExam_title());

        TextView period_tv = (TextView) findViewById(R.id.examinfo_TextView_period);
        period_tv.setText("기간 : " + mExamInfo.getExam_start_time() + " ~ " + mExamInfo.getExam_end_time());

        TextView attended_person = (TextView) findViewById(R.id.examinfo_TextView_attended_person);
        attended_person.setText("총 참여자수 : " + mExamInfo.getExam_member_count() + "명");

        RelativeLayout checkForAnswer = (RelativeLayout) findViewById(R.id.examinfo_RelativeLayout_Check_for_answer); //TODO: xml id 명명 규칙 정해야 합니다.
        checkForAnswer.setOnClickListener(this);

        RelativeLayout wrongPercent = (RelativeLayout) findViewById(R.id.examinfo_RelativeLayout_wrong_percent);
        wrongPercent.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initNetwork();
        download();

        //FIXME: 지금은 파일 입출력으로 데이터를 받는데, 나중에 로컬DB를 쓰던가 해야 합니다.
        ExamResult mExamResult = null;
        RelativeLayout result_area = (RelativeLayout) findViewById(R.id.examinfo_RelativeLayout_result_area);

        try {
            mExamResult = (ExamResult) FileStorage.getInstance(this).loadObject(mExamInfo.getExam_id());

            // 이하  FileNotFoundException이 발생하지 않은 경우.
            result_area.setVisibility(View.VISIBLE);
            TextView check_score = (TextView) findViewById(R.id.examinfo_TextView_check_score);
            check_score.setText(mExamResult.user_score + "/" + mExamResult.max_score + "점");
        } catch (FileNotFoundException e) { // 파일이 없는 경우. 충분히 예상 가능하고 전혀 문제 없음.
            result_area.setVisibility(View.INVISIBLE);
        } catch (IOException e) {
            e.printStackTrace();
            sendException(e);
            result_area.setVisibility(View.INVISIBLE); // 알릴 필요는 없고, 결과를 안보이게는 해야 한다.
        }
    }

    private void loadIntentExtras() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mExamInfo = (ExamInfo) extras.get("examInfo");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) { // FIXME case마다 각각의 함수가 필요함.
            case R.id.examinfo_RelativeLayout_Check_for_answer: {
                sendUIEvent("examinfo_RelativeLayout_Check_for_answer.onClick()", mExamInfo.getExam_id(), null);
                if (papersInfo == null) {
                    Toast.makeText(this, "시험지를 다운로드 받는 중입니다. 잠시후에 다시 시도해주세요!", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(this, OMRCardActivity.class);
                intent.putExtra("papersInfo", papersInfo);
                intent.putExtra(INTENT_KEY.EXAMINFO_OMRCARD_PAPERID, mExamInfo.getExam_id());
                startActivity(intent);
            }
            break;
            case R.id.examinfo_RelativeLayout_wrong_percent: {
                sendUIEvent("examinfo_RelativeLayout_wrong_percent.onClick()", mExamInfo.getExam_id(), null);
                Toast.makeText(this, "준비중입니다!!!", Toast.LENGTH_LONG).show();
            }
            break;
        }
    }

    @Override
    public void callback(int error, JSONObject result) {
        if (error != net.androidsea.seoultaxi.base.api.Error.SUCCESS) {
            sendException(new Exception("정답지 다운로드 실패."));
            papersInfo = null;
            return;
        }

        try {
            ArrayList<Answer> answers = new ArrayList<Answer>();
            int total_point = result.getInt("total_point");
            int data_count = result.getInt("data_count");
            JSONArray data = result.getJSONArray("data");
            for (int i = 0; i < data_count; i++) { // 각각의 Answer 초기화.
                JSONObject resultData = data.getJSONObject(i);
                String question_id = resultData.getString("question_id");
                int point = resultData.getInt("point");
                String answer = resultData.getString("answer");
                String type = resultData.getString("type");
                Answer correctAnswer = new Answer(question_id, point, answer, type);
                answers.add(correctAnswer);
            }
            papersInfo = new PapersInfo(answers, data_count, total_point);
        } catch (JSONException e) {
            e.printStackTrace();
            sendException(e);
            Toast.makeText(this, "ERROR:뭔가 잘못 되었습니다! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void download() {
        JSONObject paramDownloadAnswer = new JSONObject();
        try {
            JSONObject argument = new JSONObject();
            argument.put("user", SystemInfo.getHashedPhoneNumber(this));

            paramDownloadAnswer.put("object", String.format("/exam/%s/answer", mExamInfo.getExam_id()));
            paramDownloadAnswer.put("method", "GET");
            paramDownloadAnswer.put("argument", argument);
        } catch (JSONException e) {
            e.printStackTrace();
            sendException(e);
            Toast.makeText(this, "ERROR:뭔가 잘못 되었습니다! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }
        HttpJsonAPI.request(paramDownloadAnswer, this);
    }
}
