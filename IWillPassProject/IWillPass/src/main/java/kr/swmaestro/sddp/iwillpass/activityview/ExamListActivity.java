package kr.swmaestro.sddp.iwillpass.activityview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.HttpJsonAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kr.swmaestro.sddp.iwillpass.SystemInfo;
import kr.swmaestro.sddp.iwillpass.model.ExamInfo;
import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

public class ExamListActivity extends GoogleAnalyticsActivity implements HttpJsonAPI.CallbackListener {

    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examlist);

        ListView listView = (ListView) findViewById(R.id.examlist_ExamListView_examListView);
        listAdapter = new ListAdapter(this);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(listAdapter);

        View requestadd = findViewById(R.id.examlist_TextView_touchable_requestadd);
        requestadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(Intent.ACTION_SEND);
                String[] tos = {"lemonsagwa@gmail.com"};
                it.putExtra(Intent.EXTRA_EMAIL, tos);
                it.putExtra(Intent.EXTRA_TEXT, "==여기에 어떤 문제집(시험지) 답안을 원하시는지 입력==");
                it.putExtra(Intent.EXTRA_SUBJECT, "[나는합격이다]문제 답안지 추가 해주세요.");
                it.setType("message/rfc822");
                startActivity(Intent.createChooser(it, "Choose Email Client"));

                sendUIEvent("examlist_TextView_touchable_requestadd.onClick()", null, null);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        initNetwork();
        loadFromServer();
    }

    private void loadFromServer() {
        // TODO 유저의 입력을 막음.

        // 서버에 요청을 보냄.
        JSONObject param = new JSONObject();
        try {
            JSONObject argument = new JSONObject();
            argument.put("user", SystemInfo.getHashedPhoneNumber(this));

            param.put("object", "/exam/list");
            param.put("method", "GET");
            param.put("argument", argument);
        } catch (JSONException e) {
            e.printStackTrace();
            sendException(e);

            // Display alert to user that high scores are currently unavailable.
            Toast.makeText(this, "ERROR:뭔가 잘못 되었습니다! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }
        HttpJsonAPI.request(param, this);
    }

    @Override
    public void callback(int error, JSONObject result) { // 여기서 받는 result는 depth2입니다.
        if (error != net.androidsea.seoultaxi.base.api.Error.SUCCESS) {
            sendException(new Exception("답지목록 다운로드 실패."));
            return;
        }
        try {
            int totalCount = result.getInt("data_count");
            listAdapter.clear(); //Adapter를 비우고,
            JSONArray data = result.getJSONArray("data");
            for (int i = 0; i < totalCount; i++) {
                JSONObject resultData = data.getJSONObject(i);
                String exam_id = resultData.getString("exam_id");
                String exam_title = resultData.getString("exam_title");
                String exam_start_time = resultData.getString("exam_start_time");
                String exam_end_time = resultData.getString("exam_end_time");
                int exam_total_point = resultData.getInt("exam_total_point");
                int exam_member_count = resultData.getInt("exam_member_count");
                ExamInfo examInfo = new ExamInfo(exam_id, exam_title, exam_start_time, exam_end_time, exam_total_point, exam_member_count);
                listAdapter.add(examInfo); // 별도의 Collection 을 둘 필요 없이 바로 집어 넣습니다.
            }
            listAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
            sendException(e);
            // Display alert to user that high scores are currently unavailable.
            Toast.makeText(this, "ERROR:뭔가 잘못 되었습니다! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }
        // 유저의 입력을 허용.
    }

    class ListAdapter extends ArrayAdapter<ExamInfo> implements AdapterView.OnItemClickListener {
        private Activity activity;

        public ListAdapter(Activity activity) {
            super(activity, R.layout.listrow_examlist);
            this.activity = activity;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ExamInfo examInfo = getItem(position);

            sendUIEvent("examlist_ExamListView_examListView.onItemClick()", examInfo.getExam_id(), null);

            Intent intent = new Intent(activity, ExamInfoActivity.class);
            intent.putExtra("examInfo", examInfo); // examInfo가 있어야 할것임. (시험지 답을 받기 위해서)
            activity.startActivity(intent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ExamListRowView viewWrapper = null;
            if (row == null) { // xml 파싱을 줄이기 위해, 이전 객체 재사용.
                LayoutInflater inflater = activity.getLayoutInflater();
                row = inflater.inflate(R.layout.listrow_examlist, null);
                viewWrapper = new ExamListRowView(row);
                row.setTag(viewWrapper); // 어차피 이거 하나만 달아둘꺼니.. 별도의 Key 없이 Tag로 달아 둔다.
            } else {
                viewWrapper = (ExamListRowView) row.getTag();
            }
            ExamInfo examInfo = getItem(position);
            viewWrapper.getTextViewTitle().setText(examInfo.getExam_title());
            viewWrapper.getTextViewStartTime().setText(examInfo.getExam_start_time());
            viewWrapper.getTextViewEndTime().setText(examInfo.getExam_end_time());
            return row;
        }

        /**
         * ExamListAcrivityController의 getView에서 써먹을, Holder Pattern을 위한 ViewWrapper 입니다.
         * 하는 일은, 파싱한뷰들을 , 가비지 컬렉터가 해제 하지 않도록 들고 있는 것!
         * Created by rino0601 on 13. 9. 30..
         */
        private class ExamListRowView {
            private final View row;
            private TextView textViewTitle = null;
            private TextView textViewStartTime = null;
            private TextView textViewEndTime = null;

            public ExamListRowView(View row) {
                this.row = row;
            }

            public TextView getTextViewTitle() {
                if (textViewTitle == null) {
                    textViewTitle = (TextView) row.findViewById(R.id.textView_title);
                }
                return textViewTitle;
            }

            public TextView getTextViewStartTime() {
                if (textViewStartTime == null) {
                    textViewStartTime = (TextView) row.findViewById(R.id.textView_start_time);
                }
                return textViewStartTime;
            }

            public TextView getTextViewEndTime() {
                if (textViewEndTime == null) {
                    textViewEndTime = (TextView) row.findViewById(R.id.textView_end_time);
                }
                return textViewEndTime;
            }
        }
    }
}
