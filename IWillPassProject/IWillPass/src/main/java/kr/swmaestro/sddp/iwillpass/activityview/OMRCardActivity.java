package kr.swmaestro.sddp.iwillpass.activityview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.HttpJsonAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import kr.swmaestro.sddp.iwillpass.SystemInfo;
import kr.swmaestro.sddp.iwillpass.model.Answer;
import kr.swmaestro.sddp.iwillpass.model.ExamResult;
import kr.swmaestro.sddp.iwillpass.model.FileStorage;
import kr.swmaestro.sddp.iwillpass.model.INTENT_KEY;
import kr.swmaestro.sddp.iwillpass.model.PapersInfo;
import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

public class OMRCardActivity extends GoogleAnalyticsActivity implements HttpJsonAPI.CallbackListener {

    private PapersInfo papersInfo;
    private String papersID;
    private ListAdapter omrCardActivityController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_omr_card);

        TextView touchableQuit = (TextView) findViewById(R.id.omr_card_TextView_touchable_quit);
        touchableQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUIEvent("omr_card_TextView_touchable_quit.onClick()", papersID, null);
                finish();
            }
        });

        TextView touchableScoring = (TextView) findViewById(R.id.omr_card_TextView_touchable_scoring);
        touchableScoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUIEvent("omr_card_TextView_touchable_scoring.onClick()",
                        papersID, null);
                submit();
            }
        });
        loadIntentExtras();

        ListView listView = (ListView) findViewById(R.id.omr_card_OMRCardListView_listView);
        omrCardActivityController = new ListAdapter(this);
        listView.setAdapter(omrCardActivityController);
        omrCardActivityController.init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initNetwork();
    }

    private void loadIntentExtras() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            papersInfo = (PapersInfo) extras.get("papersInfo");
            papersID = (String) extras.get(INTENT_KEY.EXAMINFO_OMRCARD_PAPERID);
        }
        // TODO 만약 papaersInfo가 없을 경우 대비하는 코드를 이 아래에.
    }


    private void submit() {
        int totalScore = omrCardActivityController.scoringOMRCard();
        sendingResultToSever(totalScore);

        // 테스트를 위해, 결과를 토스트로 띄운다. - 실제로는 좀 다르게 표시할 것이다.
        Toast.makeText(this, String.format("총점" + papersInfo.getTotal_point() + " 중 %d점", totalScore), Toast.LENGTH_SHORT).show();
        saveExamResult(totalScore);

        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                finish();
            }
        }, 1000);
    }

    private void saveExamResult(int totalScore) {
        ExamResult mExamResult = new ExamResult(totalScore, papersInfo.getTotal_point());
        try {
            FileStorage.getInstance(this).saveObject(papersID, mExamResult);
        } catch (IOException e) {
            e.printStackTrace();
            sendException(e);

            Toast.makeText(this, "ERROR:뭔가 잘못 되었습니다! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    private void sendingResultToSever(int totalScore) {
        // 서버에 결과를 전송한다.
        JSONObject paramSendResult = new JSONObject();
        try {
            JSONObject argument = new JSONObject();
            argument.put("user", SystemInfo.getHashedPhoneNumber(this));

            JSONArray result = omrCardActivityController.getUserAnswers();
            argument.put("answers", result);
            argument.put("total_point", totalScore);

            paramSendResult.put("object", String.format("/exam/%s/answer", papersID));
            paramSendResult.put("method", "PUT");
            paramSendResult.put("argument", argument);
        } catch (JSONException e) {
            e.printStackTrace();
            sendException(e);

            // Display alert to user that high scores are currently unavailable.
            Toast.makeText(this, "ERROR:뭔가 잘못 되었습니다! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }
        HttpJsonAPI.request(paramSendResult, this);
    }


    @Override
    public void callback(int error, JSONObject result) {
        if (error != net.androidsea.seoultaxi.base.api.Error.SUCCESS) {
            sendException(new Exception("서버에 결과 보내기 실패."));
        }
    }

    public class ListAdapter extends ArrayAdapter<Answer> {
        private Map<String, String> userAnswer;
        private final Activity activity;

        public ListAdapter(Activity activity) {
            super(activity, R.layout.listrow_omr_card_objective5);
            this.activity = activity;
        }

        private void init() {
            clear();
            createOMRCard();
            notifyDataSetChanged();
        }

        private void createOMRCard() {
            userAnswer = new HashMap<String, String>(papersInfo.getData_count());
            for (Answer a : papersInfo.getAnswers()) {
                userAnswer.put(a.getQuestion_id(), "none");
                add(a);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
//        OMRCardRowView viewWrapper;
//        if (row == null) { // xml 파싱을 줄이기 위해, 이전 객체 재사용.
//            LayoutInflater inflater = activity.getLayoutInflater();
//            row = inflater.inflate(R.layout.listrow_omr_card_objective5, null);
//            if (row == null)
//                throw new IllegalStateException("inflater.inflate(R.layout.listrow_omr_card_objective5, null); -> return null");
//            viewWrapper = new OMRCardRowView(row);
//            row.setTag(viewWrapper); // 어차피 이거 하나만 달아둘꺼니.. 별도의 Key 없이 Tag로 달아 둔다.
//        } else {
//            viewWrapper = (OMRCardRowView) row.getTag();
//        }
//        Answer item = getItem(position);
//        viewWrapper.getTextViewProbNum().setText(String.format("%2s문항", item.question_id));
//        viewWrapper.getRadioGroupUserAnswer().init(this, position);


            LayoutInflater inflater = activity.getLayoutInflater();
            Answer item = getItem(position);
            if (item.getType().equals("objective5")) {
                row = inflater.inflate(R.layout.listrow_omr_card_objective5, null);
                TextView problemNum = (TextView) row.findViewById(R.id.omr_card_TextView_problemNum);
                problemNum.setText(String.format("%2s문항", item.getQuestion_id()));

                OMRCardRadioGroup objective5 = (OMRCardRadioGroup) row.findViewById(R.id.omr_card_RadioGroup_objective5);
                Integer answer = getAnswer(position);
                if (answer != null)
                    objective5.check(answer);
                final int fposition = position;
                objective5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int answer) {
                        if (answer == -1) {
                            userAnswer.put(getItem(fposition).getQuestion_id(), "none");
                            return;
                        }
                        userAnswer.put(getItem(fposition).getQuestion_id(), String.format("%d", answer));
                        sendUIEvent("omr_card_RadioGroup_objective5.onCheckedChanged()", "OMRCard_mark_button:" + answer, null);
                    }
                });
                objective5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendUIEvent("omr_card_RadioGroup_objective5.onClick()", "Miss Click", null);
                    }
                });
            } else if (item.getType().equals("subjective")) {
                row = inflater.inflate(R.layout.listrow_omr_card_subjective_math, null);
                TextView problemNum = (TextView) row.findViewById(R.id.omr_card_TextView_problemNum);
                problemNum.setText(String.format("%2s문항", item.getQuestion_id()));

                OMRCardRadioGroupMath subjectiveMath = (OMRCardRadioGroupMath) row.findViewById(R.id.omr_card_RadioGroup_subjective_math);
                Integer answer = getAnswer(position);
                if (answer != null)
                    subjectiveMath.setSelect(answer);
                final int fposition = position;
                subjectiveMath.setOnCheckedChangeListener(new OMRCardRadioGroupMath.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(OMRCardRadioGroupMath group, int value) {
                        if (value == -1) {
                            userAnswer.put(getItem(fposition).getQuestion_id(), "none");
                            return;
                        }
                        userAnswer.put(getItem(fposition).getQuestion_id(), String.format("%d", value));
                    }
                });
                subjectiveMath.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendUIEvent("omr_card_RadioGroup_subjective_math.onClick()", "Miss Click", null);
                    }
                });

            } else {
                row = inflater.inflate(R.layout.listrow_omr_card_subjective_edittext, null);
                TextView problemNum = (TextView) row.findViewById(R.id.omr_card_TextView_problemNum);
                problemNum.setText(String.format("%2s문항", item.getQuestion_id()));
                EditText answer = (EditText) row.findViewById(R.id.omr_card_EditText_answer);
                answer.setText("" + userAnswer.get(item.getQuestion_id()));
                // FIXME EditText 쓰는 주관식 입력기는 약간 임시의 성향이 강해서 클래스를 따로 빼지않음. 정식 UI가 된다면 클래스를 따로 뺄 것.
                TextWatcher watcher = new TextWatcher() {
                    private Answer item = null;

                    public TextWatcher setItem(Answer item) {
                        this.item = item;
                        return this;
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        //텍스트 변경 후 발생할 이벤트를 작성.
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        //텍스트의 길이가 변경되었을 경우 발생할 이벤트를 작성.
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        //텍스트가 변경될때마다 발생할 이벤트를 작성.
                        userAnswer.put(item.getQuestion_id(), s.toString());
                    }
                }.setItem(item);
                answer.addTextChangedListener(watcher);
            }

            return row;
        }

        public class OMRCardRowView {
            public View row;
            public TextView textViewProbNum = null;
            public OMRCardRadioGroup radioGroupUserAnswer = null;

            public OMRCardRowView(View row) {
                this.row = row;
            }

            public TextView getTextViewProbNum() {
                if (textViewProbNum == null) {
                    textViewProbNum = (TextView) row.findViewById(R.id.omr_card_TextView_problemNum);
                }
                return textViewProbNum;
            }

            public OMRCardRadioGroup getRadioGroupUserAnswer() {
                if (radioGroupUserAnswer == null) {
                    radioGroupUserAnswer = (OMRCardRadioGroup) row.findViewById(R.id.omr_card_RadioGroup_objective5);
                }
                return radioGroupUserAnswer;
            }
        }

        private int scoringOMRCard() {
            int totalScore = 0;
            for (int i = 0; i < papersInfo.getData_count(); i++) {
                Answer correctAnswer = getItem(i);
                if (userAnswer.get(correctAnswer.getQuestion_id()).equals(correctAnswer.getAnswer())) { // 맞으면,
                    totalScore += correctAnswer.getPoint(); // 총점 상승.
                }
            }
            return totalScore;
        }

        private JSONArray getUserAnswers() throws JSONException {
            JSONArray result = new JSONArray();
            for (int i = 0; i < papersInfo.getData_count(); i++) {
                Answer a = getItem(i);
                JSONObject jsonA = new JSONObject();
                jsonA.put("question_id", a.getQuestion_id());
                jsonA.put("point", a.getPoint());
                jsonA.put("answer", userAnswer.get(a.getQuestion_id())); // 이것만 교체!.
                jsonA.put("type", a.getType());
                result.put(jsonA);
            }
            return result;
        }

        private Integer getAnswer(int position) {
            Answer item = getItem(position);
            String answer = userAnswer.get(item.getQuestion_id());
            if (answer.equals("none"))
                return null;
            return Integer.parseInt(answer);
        }
    }
}


