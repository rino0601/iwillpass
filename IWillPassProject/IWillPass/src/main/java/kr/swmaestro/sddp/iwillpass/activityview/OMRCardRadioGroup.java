package kr.swmaestro.sddp.iwillpass.activityview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class OMRCardRadioGroup extends RadioGroup {

    public OMRCardRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        create(context);
    }

    private void create(Context context) {
        String tagstr = (String) getTag();
        int count = Integer.parseInt(tagstr);

        RadioGroup.LayoutParams wcwc = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RadioGroup.LayoutParams heightMatch = new LayoutParams(40, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
        RadioGroup.LayoutParams widthMatch = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        RadioGroup.LayoutParams linearParams = getOrientation() == HORIZONTAL ? heightMatch : widthMatch;

        int id;
        for (id = 0; id < count - 1; id++) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setLayoutParams(wcwc);
            radioButton.setId(id + 1);
            addView(radioButton);

            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(linearParams);
            linearLayout.setClickable(false);
            addView(linearLayout);
        }
        RadioButton radioButton = new RadioButton(context);
        radioButton.setLayoutParams(wcwc);
        radioButton.setId(id + 1);
        addView(radioButton);
    }
}
