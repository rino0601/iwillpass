package kr.swmaestro.sddp.iwillpass.activityview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

public class OMRCardRadioGroupMath extends RadioGroup implements RadioGroup.OnCheckedChangeListener {
    private Context context;
    private AttributeSet attrs;

    private OnCheckedChangeListener onCheckedChangeListener;
    private int[] valueHolder;
    private OMRCardRadioGroup[] omrCardRadioGroups;

    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    public interface OnCheckedChangeListener {
        public void onCheckedChanged(OMRCardRadioGroupMath group, int value);
    }

    public OMRCardRadioGroupMath(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attrs = attrs;
        create(context);
    }

    private void create(Context context) {
        this.context = context;

        valueHolder = new int[3];
        omrCardRadioGroups = new OMRCardRadioGroup[3];

        if (getOrientation() != HORIZONTAL)
            throw new IllegalArgumentException("orientation must be horizontal!");

        RadioGroup.LayoutParams wcmp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        TextView[] label = new TextView[3];
        label[0] = new TextView(context);
        label[0].setLayoutParams(wcmp);
        label[1] = new TextView(context);
        label[1].setLayoutParams(wcmp);
        label[2] = new TextView(context);
        label[2].setLayoutParams(wcmp);

        label[0].setText("백단위");
        label[1].setText("십단위");
        label[2].setText("일단위");

        for (int i = 0; i < 3; i++) {
            OMRCardRadioGroup omrCardRadioGroup = new OMRCardRadioGroup(context, attrs);
            omrCardRadioGroup.setOrientation(VERTICAL);
            omrCardRadioGroup.addView(label[i], 0);
            omrCardRadioGroup.setId(i);
            omrCardRadioGroup.setOnCheckedChangeListener(this);
            addView(omrCardRadioGroup);
            omrCardRadioGroups[i] = omrCardRadioGroup;
        }
    }

    @Override
    public void clearCheck() {
        super.clearCheck();
        valueHolder = new int[3];
    }

    public void setSelect(int value) {
        int v100 = value / 100;
        int v10 = value / 10 % 10;
        int v1 = value % 10;
        valueHolder[0] = v100;
        valueHolder[1] = v10;
        valueHolder[2] = v1;
        for (int i = 0; i < 3; i++) {
            omrCardRadioGroups[i].check(valueHolder[i]);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        valueHolder[group.getId()] = checkedId;
        if (onCheckedChangeListener != null) {
            onCheckedChangeListener.onCheckedChanged(this, (valueHolder[0] * 100 + valueHolder[1] * 10 + valueHolder[2]));
        }
    }
}
