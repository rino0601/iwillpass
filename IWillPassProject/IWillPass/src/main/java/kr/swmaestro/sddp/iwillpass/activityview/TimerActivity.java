package kr.swmaestro.sddp.iwillpass.activityview;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dulgi.MoGye.dev.R;

import java.util.Date;

import kr.swmaestro.sddp.iwillpass.view.GoogleAnalyticsActivity;

public class TimerActivity extends GoogleAnalyticsActivity {
    int timer_set = 1;
    int stop_counter = 0;
    boolean startnpause_set = false;
    long korean = 4800; // 80��
    long math = 6000;// 100��
    long english = 4200;// 70��
    long tamgu = 1800;// 30��
    long second;
    long diff_time;
    long diff_time_second;
    long save_diff_time_second;
    long surplus_second;
    long surplus_second2;
    Date click_time;
    Date reclick_time;
    Date now_time;
    Date now_time2;

    RadioGroup radio;
    private Handler handler = new Handler();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_timer);

        final TextView test_timer = (TextView) findViewById(R.id.test_timer);
        final TextView my_dajim_text = (TextView) findViewById(R.id.my_dajim_text);
        final Button timer_startnpause = (Button) findViewById(R.id.timer_startnpause);
        final Button timer_reset = (Button) findViewById(R.id.timer_reset);
        final CheckBox listen_exam = (CheckBox) findViewById(R.id.listen_exam);

        final Typeface robotomedium = Typeface.createFromAsset(getAssets(),
                "fonts/Roboto-Regular.ttf");
        test_timer.setTypeface(robotomedium);

        radio = (RadioGroup) findViewById(R.id.radiogroup_timer);
        RadioButton radioboutton1 = (RadioButton) findViewById(R.id.korean_timer);
        RadioButton radioboutton2 = (RadioButton) findViewById(R.id.math_timer);
        RadioButton radioboutton3 = (RadioButton) findViewById(R.id.english_timer);
        RadioButton radioboutton4 = (RadioButton) findViewById(R.id.tamgu_timer);
        final Typeface bareundotumotf2 = Typeface.createFromAsset(getAssets(),
                "fonts/bareundotumotf2.otf");
        my_dajim_text.setTypeface(bareundotumotf2);
        radioboutton1.setTypeface(bareundotumotf2);
        radioboutton2.setTypeface(bareundotumotf2);
        radioboutton3.setTypeface(bareundotumotf2);
        radioboutton4.setTypeface(bareundotumotf2);
        listen_exam.setTypeface(bareundotumotf2);

        test_timer.setText("80:00");
        timer_set = 1;
        second = korean;

        timer_startnpause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (startnpause_set == false) {// �ð谡 ������������ ������
                    if ((timer_set == 1) || (timer_set == 3)) {
                        listen_exam.setVisibility(View.INVISIBLE);
                    }
                    if (stop_counter == 0) {
                        click_time = new Date();
                        startnpause_set = true;
                        radio.setVisibility(View.INVISIBLE);
                        handler.postDelayed(doUpdateTimer, 1000);
                    } else if (stop_counter > 0) {
                        startnpause_set = true;
                        reclick_time = new Date();
                        radio.setVisibility(View.INVISIBLE);
                        handler.postDelayed(doUpdateTimer, 1000);
                    }

                } else if (startnpause_set == true) {// �ð谡 ��������
                    if ((timer_set == 1) || (timer_set == 3)) {
                        listen_exam.setVisibility(View.VISIBLE);
                    }

                    handler.removeCallbacks(doUpdateTimer);
                    save_diff_time_second = diff_time_second;
                    stop_counter = stop_counter + 1;
                    startnpause_set = false;
                    radio.setVisibility(View.VISIBLE);
                }
            }
        });

        timer_reset.setOnClickListener(new View.OnClickListener() {// �ð� �ʱ�ȭ
            public void onClick(View v) {
                radio.setVisibility(View.VISIBLE);
                startnpause_set = false;
                stop_counter = 0;
                if (listen_exam.isChecked() == false) {
                    if (timer_set == 1) {
                        test_timer.setText("80:00");
                        listen_exam.setVisibility(View.VISIBLE);
                    } else if (timer_set == 2) {
                        test_timer.setText("100:00");

                    } else if (timer_set == 3) {
                        test_timer.setText("70:00");
                        listen_exam.setVisibility(View.VISIBLE);

                    } else if (timer_set == 4) {
                        test_timer.setText("30:00");
                    }
                } else if (listen_exam.isChecked() == true) {
                    if (timer_set == 1) {
                        test_timer.setText("68:00");
                        listen_exam.setVisibility(View.VISIBLE);

                    } else if (timer_set == 2) {
                        test_timer.setText("100:00");

                    } else if (timer_set == 3) {
                        test_timer.setText("50:00");
                        listen_exam.setVisibility(View.VISIBLE);

                    } else if (timer_set == 4) {
                        test_timer.setText("30:00");
                    }
                }
                handler.removeCallbacks(doUpdateTimer);
            }
        });

        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                TextView test_timer = (TextView) findViewById(R.id.test_timer);
                if (group == radio) {

                    if (checkedId == R.id.korean_timer) {
                        test_timer.setText("80:00");
                        timer_set = 1;
                        second = korean;
                        stop_counter = 0;
                        listen_exam.setVisibility(View.VISIBLE);
                        listen_exam.setChecked(false);
                    } else if (checkedId == R.id.math_timer) {
                        test_timer.setText("100:00");
                        timer_set = 2;
                        second = math;
                        stop_counter = 0;
                        listen_exam.setVisibility(View.INVISIBLE);

                    } else if (checkedId == R.id.english_timer) {
                        test_timer.setText("70:00");
                        timer_set = 3;
                        second = english;
                        stop_counter = 0;
                        listen_exam.setVisibility(View.VISIBLE);
                        listen_exam.setChecked(false);

                    } else if (checkedId == R.id.tamgu_timer) {
                        test_timer.setText("30:00");
                        timer_set = 4;
                        second = tamgu;
                        stop_counter = 0;
                        listen_exam.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        listen_exam
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (timer_set == 1) {
                            if (isChecked) {
                                test_timer.setText("68:00");
                                second = 4080;

                            } else {
                                test_timer.setText("80:00");
                                second = 4800;
                            }
                        } else if (timer_set == 3) {
                            if (isChecked) {
                                test_timer.setText("50:00");
                                second = 3000;

                            } else {
                                test_timer.setText("70:00");
                                second = 4200;
                            }

                        }

                    }
                });

    }

    public Runnable doUpdateTimer = new Runnable() {
        public void run() {
            Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (stop_counter == 0) {
                TextView test_timer = (TextView) findViewById(R.id.test_timer);

                now_time = new Date();
                diff_time = now_time.getTime() - click_time.getTime();
                diff_time_second = diff_time / 1000;
                surplus_second = second - diff_time_second;
                if (surplus_second <= 0) {
                    handler.removeCallbacks(doUpdateTimer);
                    test_timer.setText("��!");
                    vibe.vibrate(2000);
                } else {
                    test_timer.setText((surplus_second / 60) + ":"
                            + (surplus_second % 60));
                    handler.postDelayed(doUpdateTimer, 1000);
                }
            } else if (stop_counter > 0) {
                TextView test_timer = (TextView) findViewById(R.id.test_timer);

                now_time2 = new Date();
                diff_time = now_time2.getTime() - reclick_time.getTime();
                diff_time_second = save_diff_time_second + (diff_time / 1000);
                surplus_second2 = second - diff_time_second;
                if (surplus_second2 <= 0) {
                    handler.removeCallbacks(doUpdateTimer);
                    test_timer.setText("��!");
                    vibe.vibrate(2000);
                } else {
                    test_timer.setText((surplus_second2 / 60) + ":"
                            + (surplus_second2 % 60));
                    handler.postDelayed(doUpdateTimer, 1000);
                }
            }
        }

    };

}