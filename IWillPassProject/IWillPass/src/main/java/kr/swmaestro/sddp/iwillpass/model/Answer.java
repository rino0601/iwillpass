package kr.swmaestro.sddp.iwillpass.model;

import java.io.Serializable;

/**
 * Created by rino0601 on 13. 9. 30..
 */
public class Answer implements Serializable {
    static final String TYPE_OBJECTIVE5 = "objective5";
    static final String TYPE_SUBJECTIVE = "subjective";

    private String question_id;
    private int point;
    private String answer;
    private String type;

    public Answer(String question_id, int point, String answer, String type) {
        this.question_id = question_id;
        this.point = point;
        this.answer = answer;
        this.type = type;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public int getPoint() {
        return point;
    }

    public String getAnswer() {
        return answer;
    }

    public String getType() {
        return type;
    }
}
