package kr.swmaestro.sddp.iwillpass.model;

/**
 * Created by FlaShilver on 2013. 11. 13..
 */
public class AwordModel {
    private String schoolname;
    private String entreatyweek;
    private String passrank;
    private String passpercent;
    private String scoregrade;

    public AwordModel(String schoolname, String entreatyweek, String passrank, String passpercent, String scoregrade) {
        this.schoolname = schoolname;
        this.entreatyweek = entreatyweek;
        this.passrank = passrank;
        this.passpercent = passpercent;
        this.scoregrade = scoregrade;
    }

    public String getSchoolname() {
        return schoolname;
    }

    public void setSchoolname(String schoolname) {
        this.schoolname = schoolname;
    }

    public String getEntreatyweek() {
        return entreatyweek;
    }

    public void setEntreatyweek(String entreatyweek) {
        this.entreatyweek = entreatyweek;
    }

    public String getPassrank() {
        return passrank;
    }

    public void setPassrank(String passrank) {
        this.passrank = passrank;
    }

    public String getPasspercent() {
        return passpercent;
    }

    public void setPasspercent(String passpercent) {
        this.passpercent = passpercent;
    }

    public String getScoregrade() {
        return scoregrade;
    }

    public void setScoregrade(String scoregrade) {
        this.scoregrade = scoregrade;
    }
}


