package kr.swmaestro.sddp.iwillpass.model;

/**
 * Created by FlaShilver on 2013. 11. 13..
 */
public class Config {

    public static Config config = new Config();
    private String market_url;
    private int active_user_count;
    private String apk_upgrade_message;
    private int apk_version;
    private int target_user_count;
    private String question_deadline;
    private boolean apk_upgrade_force;

//    public Config(String market_url, int active_user_count, String apk_upgrade_message, int apk_version, int target_user_count, String question_deadline, boolean apk_upgrade_force) {
//        this.market_url = market_url;
//        this.active_user_count = active_user_count;
//        this.apk_upgrade_message = apk_upgrade_message;
//        this.apk_version = apk_version;
//        this.target_user_count = target_user_count;
//        this.question_deadline = question_deadline;
//        this.apk_upgrade_force = apk_upgrade_force;
//   }

    public static Config getConfig() {
        return config;
    }

    public static void setConfig(Config config) {
        Config.config = config;
    }

    public String getMarket_url() {
        return market_url;
    }

    public void setMarket_url(String market_url) {
        this.market_url = market_url;
    }

    public int getActive_user_count() {
        return active_user_count;
    }

    public void setActive_user_count(int active_user_count) {
        this.active_user_count = active_user_count;
    }

    public String getApk_upgrade_message() {
        return apk_upgrade_message;
    }

    public void setApk_upgrade_message(String apk_upgrade_message) {
        this.apk_upgrade_message = apk_upgrade_message;
    }

    public int getApk_version() {
        return apk_version;
    }

    public void setApk_version(int apk_version) {
        this.apk_version = apk_version;
    }

    public int getTarget_user_count() {
        return target_user_count;
    }

    public void setTarget_user_count(int target_user_count) {
        this.target_user_count = target_user_count;
    }

    public String getQuestion_deadline() {
        return question_deadline;
    }

    public void setQuestion_deadline(String question_deadline) {
        this.question_deadline = question_deadline;
    }

    public boolean isApk_upgrade_force() {
        return apk_upgrade_force;
    }

    public void setApk_upgrade_force(boolean apk_upgrade_force) {
        this.apk_upgrade_force = apk_upgrade_force;
    }


}
