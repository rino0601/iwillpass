package kr.swmaestro.sddp.iwillpass.model;

import java.io.Serializable;

/**
 * 시험지에 관한 정보를 담고 있는 model 클래스 입니다.
 * 멤버 변수가 모두 public인 이유는, 불필요한 getter를 만들고 싶지 않았기 때문입니다.
 * 어차미 모두 final로 선언해서, 다른곳에서 수정할일도 없고 하니, 이래도 되지 않을까 하는데...
 * <p/>
 * Created by rino0601 on 13. 9. 30..
 */
public class ExamInfo implements Serializable {
    private String exam_id;
    private String exam_title;
    private String exam_start_time;
    private String exam_end_time;
    private int exam_total_point;
    private int exam_member_count;

    public ExamInfo(String exam_id, String exam_title, String exam_start_time, String exam_end_time, int exam_total_point, int exam_member_count) {
        this.exam_id = exam_id;
        this.exam_title = exam_title;
        this.exam_start_time = exam_start_time;
        this.exam_end_time = exam_end_time;
        this.exam_total_point = exam_total_point;
        this.exam_member_count = exam_member_count;
    }

    public String getExam_id() {
        return exam_id;
    }

    public String getExam_title() {
        return exam_title;
    }

    public String getExam_start_time() {
        return exam_start_time;
    }

    public String getExam_end_time() {
        return exam_end_time;
    }

    public int getExam_total_point() {
        return exam_total_point;
    }

    public int getExam_member_count() {
        return exam_member_count;
    }
}
