package kr.swmaestro.sddp.iwillpass.model;

import java.io.Serializable;

/**
 * Created by FlaShilver on 2013. 10. 1..
 */
public class ExamResult implements Serializable {
    public int user_score;
    public int max_score;

    public ExamResult(int user_score, int max_score) {
        this.user_score = user_score;
        this.max_score = max_score;
    }
}
