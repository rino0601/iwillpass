package kr.swmaestro.sddp.iwillpass.model;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by rino0601 on 13. 10. 1..
 */
public class FileStorage {
    public static FileStorage instance;
    private File tempDirectory = null;

    private FileStorage() {
    }

    public static FileStorage getInstance(Context context_) {
        if (instance == null) {
            File sdcard = context_.getExternalFilesDir(null);
            instance = new FileStorage();
            instance.tempDirectory = new File(sdcard, "temp");
            if (!instance.tempDirectory.isDirectory()) {
                instance.tempDirectory.mkdirs();
            }
        }
        return instance;
    }

    public File getTempDirectory() {
        return tempDirectory;
    }

    public Object loadObject(String fileName) throws IOException {
        File fileToLoad;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Object loaded = null;
        try {
            fileToLoad = new File(getTempDirectory(), fileName);
            fis = new FileInputStream(fileToLoad);
            ois = new ObjectInputStream(fis);
            loaded = ois.readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException("class not found");
        } finally {
            if (ois != null) {
                ois.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return loaded;
    }

    public void saveObject(String fileName, Object object) throws IOException {
        File fileToSave;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fileToSave = new File(getTempDirectory(), fileName);
            fos = new FileOutputStream(fileToSave);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }
}

