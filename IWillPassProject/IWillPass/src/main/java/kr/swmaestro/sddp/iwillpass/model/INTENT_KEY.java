package kr.swmaestro.sddp.iwillpass.model;

/**
 * 인텐트 키만 다루는 public static class 입니다. R.java 처럼 쓴다고 생각하시면 됩니다.
 * 변수 네이밍 방법은,
 * public static final String [보내는 액티비티의 축약형]_[받는 액티비티의 축약형]_[대문자화한 변수명]
 * 으로 통일해주시면 됩니다.
 * 축약형이라 함은, 예를 들어 ExamInfoActivity가 있다고 하면, EXAMINFO가 됩니다.
 * Created by rino0601 on 13. 10. 4..
 */
public class INTENT_KEY {
    public static final String EXAMINFO_OMRCARD_PAPERID = "EXAMINFO_OMRCARD_PAPERID";
}
