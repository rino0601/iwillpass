package kr.swmaestro.sddp.iwillpass.model;

import java.io.Serializable;
import java.util.List;

/**
 * 서버에서 받아온 시험지 정보 입니다.
 * Created by rino0601 on 13. 10. 1..
 */
public class PapersInfo implements Serializable {

    private List<Answer> answers;
    private int data_count;
    private int total_point;

    public PapersInfo(List<Answer> answers, int data_count, int total_point) {
        this.answers = answers;
        this.data_count = data_count;
        this.total_point = total_point;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public int getData_count() {
        return data_count;
    }

    public int getTotal_point() {
        return total_point;
    }
}
