package kr.swmaestro.sddp.iwillpass.model;

import java.io.Serializable;

public class SimulationExamData implements Serializable {
    private String id;
    private String answer;
    private String questionImageUrl;
    private int point;
    private String type;

    public SimulationExamData(String id, String answer, String questionImageUrl, int point, String type) {
        this.id = id;
        this.answer = answer;
        this.questionImageUrl = questionImageUrl;
        this.point = point;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public String getQuestionImageUrl() {
        return questionImageUrl;
    }

    public int getPoint() {
        return point;
    }

    public String getType() {
        return type;
    }
}
