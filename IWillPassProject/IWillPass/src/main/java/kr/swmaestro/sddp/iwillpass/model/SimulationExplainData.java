package kr.swmaestro.sddp.iwillpass.model;

import java.io.Serializable;

/**
 * Created by rino0601 on 2013. 11. 13..
 */
public class SimulationExplainData implements Serializable {
    private String id;
    private String answer;
    private String questionImageUrl;
    private String explainImageUrl;
    private int errorRate;
    private int point;
    private String type;

    public SimulationExplainData(String id, String answer, String questionImageUrl, String answerImageUrl, int errorRate, int point, String type) {
        this.id = id;
        this.answer = answer;
        this.questionImageUrl = questionImageUrl;
        this.explainImageUrl = answerImageUrl;
        this.errorRate = errorRate;
        this.point = point;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public String getQuestionImageUrl() {
        return questionImageUrl;
    }

    public String getExplainImageUrl() {
        return explainImageUrl;
    }

    public int getErrorRate() {
        return errorRate;
    }

    public int getPoint() {
        return point;
    }

    public String getType() {
        return type;
    }
}
