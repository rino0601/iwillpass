package kr.swmaestro.sddp.iwillpass.model;

import android.content.Context;

import java.io.Serializable;

import kr.swmaestro.sddp.iwillpass.util.SqliteDBUtil;

/**
 * Created by rino0601 on 2013. 11. 13..
 */
public class SimulationUserAnswerData implements Serializable {

    private String id;
    private String answer;
    private long time;

    public SimulationUserAnswerData(String id, String answer, long time) {
        this.id = id;
        this.answer = answer;
        this.time = time;
    }

    public static void save(Context context, SimulationUserAnswerData data) {
        SqliteDBUtil sqliteDBUtil = new SqliteDBUtil(context);
        sqliteDBUtil.write(SimulationUserAnswerData.class.getName(), data.getId(), data);
    }

    public static SimulationUserAnswerData load(Context context, String id) {
        SqliteDBUtil sqliteDBUtil = new SqliteDBUtil(context);
        return (SimulationUserAnswerData) sqliteDBUtil.read(SimulationUserAnswerData.class.getName(), id);
    }

    public String getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public long getTime() {
        return time;
    }
}
