package kr.swmaestro.sddp.iwillpass.model;

import java.util.Date;

/**
 * Created by FlaShilver on 2013. 11. 14..
 */
public class StudyTimerModel {

    public static StudyTimerModel studyTimerModel = new StudyTimerModel();

    private long korean_studytime = 0;
    private long math_studytime = 0;
    private long english_studytime = 0;
    private long tamgu_studytime = 0;
    private long all_studytime = 0;
    private int now_subject = 1;
    private Date pick_timer = null;
    private boolean now_working = false;
    private long save_diff_time = 0;

    public long getSave_diff_time() {
        return save_diff_time;
    }

    public void setSave_diff_time(long save_diff_time) {
        this.save_diff_time = save_diff_time;
    }

    public int getNow_subject() {
        return now_subject;
    }

    public void setNow_subject(int now_subject) {
        this.now_subject = now_subject;
    }

    public Date getPick_timer() {
        return pick_timer;
    }

    public void setPick_timer(Date pick_timer) {
        this.pick_timer = pick_timer;
    }

    public boolean isNow_working() {
        return now_working;
    }

    public void setNow_working(boolean now_working) {
        this.now_working = now_working;
    }

    public long getAll_studytime() {
        return all_studytime;
    }

    public void setAll_studytime(long all_studytime) {
        this.all_studytime = all_studytime;
    }

    public long getKorean_studytime() {
        return korean_studytime;
    }

    public void setKorean_studytime(long korean_studytime) {
        this.korean_studytime = korean_studytime;
    }

    public long getMath_studytime() {
        return math_studytime;
    }

    public void setMath_studytime(long math_studytime) {
        this.math_studytime = math_studytime;
    }

    public long getEnglish_studytime() {
        return english_studytime;
    }

    public void setEnglish_studytime(long english_studytime) {
        this.english_studytime = english_studytime;
    }

    public long getTamgu_studytime() {
        return tamgu_studytime;
    }

    public void setTamgu_studytime(long tamgu_studytime) {
        this.tamgu_studytime = tamgu_studytime;
    }

    public void saveStudyTime(long studytime) {
        studytime = studytime / 1000; //studytime을 초단위로 변경

        if (this.now_subject == 1) {
            studyTimerModel.setKorean_studytime(studytime);

        } else if (this.now_subject == 2) {
            studyTimerModel.setMath_studytime(studytime);

        } else if (this.now_subject == 3) {
            studyTimerModel.setEnglish_studytime(studytime);

        } else if (this.now_subject == 4) {
            studyTimerModel.setTamgu_studytime(studytime);
        }
        studyTimerModel.setSave_diff_time(studytime);
    }

    public long initializeTimer(int subject) {
        if (subject == 1) {
            studyTimerModel.setKorean_studytime(0);
            this.all_studytime = this.korean_studytime + this.math_studytime + this.english_studytime + this.tamgu_studytime;
            return korean_studytime;

        } else if (subject == 2) {
            studyTimerModel.setMath_studytime(0);
            this.all_studytime = this.korean_studytime + this.math_studytime + this.english_studytime + this.tamgu_studytime;
            return math_studytime;


        } else if (subject == 3) {
            studyTimerModel.setEnglish_studytime(0);
            this.all_studytime = this.korean_studytime + this.math_studytime + this.english_studytime + this.tamgu_studytime;
            return english_studytime;


        } else {
            studyTimerModel.setTamgu_studytime(0);
            this.all_studytime = this.korean_studytime + this.math_studytime + this.english_studytime + this.tamgu_studytime;
            return tamgu_studytime;
        }
    }

    public void saveAllStaus() {
        if (this.now_subject == 1) {

        } else if (this.now_subject == 2) {

        } else if (this.now_subject == 3) {

        } else if (this.now_subject == 4){

        }
    }
}
