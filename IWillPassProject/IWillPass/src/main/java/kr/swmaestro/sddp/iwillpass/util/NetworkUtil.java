package kr.swmaestro.sddp.iwillpass.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by rino0601 on 2013. 10. 28..
 */
public class NetworkUtil {
    public static final int NETWORK_WIFI = 0;
    public static final int NETWORK_3G = 1;
    public static final int NETWORK_NONE = 2;

    public static int checkStatus(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi != null && wifi.isAvailable()) {
            return NETWORK_WIFI;
        } else if (mobile != null && mobile.isAvailable()) {
            return NETWORK_3G;
        } else {
            return NETWORK_NONE;
        }
    }
}
