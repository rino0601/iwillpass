package kr.swmaestro.sddp.iwillpass.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by rino0601 on 2013. 11. 14..
 */
public class SqliteDBUtil extends SQLiteOpenHelper {

    private static final String table = "SerializedObject";
    private static final String class_type = "class_type";
    private static final String object_id = "object_id";
    private static final String data = "data";
    private static final String time_stamp = "time_stamp";

    public SqliteDBUtil(Context context) {
        super(context, "serialized_object.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + table + "(" + class_type + " TEXT NOT NULL, " + object_id + " TEXT NOT NULL, " + data + " BLOB, " + time_stamp + " NUMERIC NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }

    public Object read(String type, String id) {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor cusor = null;
        cusor = readableDatabase.query(table, new String[]{class_type, object_id, data}, class_type + "='" + type + "' AND " + object_id + "='" + id + "'", null, null, null, null);
        byte[] dataBlob = null;
        cusor.moveToFirst();
        dataBlob = cusor.getBlob(cusor.getColumnIndex(data));
        cusor.close();
        readableDatabase.close();

        ObjectInputStream ois = null;
        Object loaded = null;
        try {
            ByteArrayInputStream input = new ByteArrayInputStream(dataBlob);
            ois = new ObjectInputStream(input);
            loaded = ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {/*ignore exception*/}
            }
        }
        return loaded;
    }

    public void write(String type, String id, Object object) {
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;
        byte[] dataByte = null;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            dataByte = baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (oos != null)
                try {
                    oos.close();
                } catch (IOException e) {/*ignore Exception*/}
            if (baos != null)
                try {
                    baos.close();
                } catch (IOException e) {/*ignore Exception*/}
        }

        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues row = new ContentValues();
        row.put(class_type, type);
        row.put(object_id, id);
        row.put(data, dataByte);
        row.put(time_stamp, System.currentTimeMillis());
        writableDatabase.insert(table, null, row);
        writableDatabase.close();
    }
}
