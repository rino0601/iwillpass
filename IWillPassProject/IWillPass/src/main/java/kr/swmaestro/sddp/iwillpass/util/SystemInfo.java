package kr.swmaestro.sddp.iwillpass.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Base64;

import net.androidsea.seoultaxi.base.util.GMLog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jaehwacho on 13. 9. 5..
 */
public class SystemInfo {
    private static String getPhoneNumber(Context context) {
        if (context == null)
            return null;

        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String num = telManager.getLine1Number();

        if (num == null) {
            num = "01000000000";
        }

        GMLog.d(SystemInfo.class.getName(), "getPhoneNumber " + num);
        return num;
    }

    public static String getHashedPhoneNumber(Context context) {
        // 여기에 자기 hash번호 쓰면 좋을 것 같네요.
        // 고한종 - nexus7 : xo0nGM0+OKSsqmaGmUGfDQ
        // 김종찬 -
        // 조재화 -
        if (context == null)
            return null;

        String line1Num = getPhoneNumber(context);

        MessageDigest md_md5 = null;
        try {
            md_md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

        md_md5.update(line1Num.getBytes());
        byte[] rawNum = md_md5.digest();
        String udid = Base64.encodeToString(rawNum, Base64.NO_PADDING | Base64.NO_WRAP);
        return udid;
    }

}
