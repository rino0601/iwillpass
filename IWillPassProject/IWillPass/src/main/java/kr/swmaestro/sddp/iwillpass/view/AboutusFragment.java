package kr.swmaestro.sddp.iwillpass.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

import kr.swmaestro.sddp.iwillpass.model.Config;
import kr.swmaestro.sddp.iwillpass.model.StudyTimerModel;

/**
 * Created by FlaShilver on 2013. 11. 11..
 */
public class AboutusFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.about_us, container, false);

        Toast.makeText(getActivity(),""+ StudyTimerModel.studyTimerModel.getKorean_studytime()+","+StudyTimerModel.studyTimerModel.getMath_studytime()+","+StudyTimerModel.studyTimerModel.getEnglish_studytime(),1000).show();
        return rootView;
    }
}
