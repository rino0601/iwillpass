package kr.swmaestro.sddp.iwillpass.view;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dulgi.MoGye.dev.R;

import java.util.ArrayList;

import kr.swmaestro.sddp.iwillpass.model.AwordModel;

/**
 * Created by FlaShilver on 2013. 11. 13..
 */
public class AwordFragment extends Fragment {


    ArrayList<AwordModel> lists;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmentaword, container, false);

        ListView listview = (ListView) rootView.findViewById(R.id.listview);
        lists = new ArrayList<AwordModel>();
        EntreatyRecordAdapter entreatyrecordAdapter = new EntreatyRecordAdapter(getActivity(), R.layout.row_aword, lists);


        lists.add(new AwordModel("서울대(일반)", "2014년 3월 2주차 가상입시 결과", "합격석차:64위", "백분율:95%", "내신등급:1.10"));
        lists.add(new AwordModel("연세대(수석)", "2014년 3월 1주차 가상입시 결과", "합격석차:4위", "백분율:96%", "내신등급:1.12"));

        listview.setAdapter(entreatyrecordAdapter);

        return rootView;

    }

    private class EntreatyRecordAdapter extends ArrayAdapter<AwordModel> {

        public ArrayList<AwordModel> items;
        private Context mcontext;

        public EntreatyRecordAdapter(Context context, int resource, ArrayList<AwordModel> items) {
            super(context, resource, items);
            mcontext = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_aword, null);
            }

            AwordModel areaInfo = items.get(position);

            if (areaInfo != null) {
                TextView schoolname = (TextView) v.findViewById(R.id.school_name);
                TextView entreatyweek = (TextView) v.findViewById(R.id.entreatyweek);
                TextView passrank = (TextView) v.findViewById(R.id.passrank);
                TextView passpercent = (TextView) v.findViewById(R.id.passpercent);
                TextView scoregrade = (TextView) v.findViewById(R.id.scoregrade);

                schoolname.setText(areaInfo.getSchoolname());
                entreatyweek.setText(areaInfo.getEntreatyweek());
                passrank.setText(areaInfo.getPassrank());
                passpercent.setText(areaInfo.getPasspercent());
                scoregrade.setText(areaInfo.getScoregrade());
            }

            return v;
        }
    }
}
