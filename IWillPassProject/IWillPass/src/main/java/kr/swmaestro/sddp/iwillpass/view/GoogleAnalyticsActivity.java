package kr.swmaestro.sddp.iwillpass.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.dulgi.MoGye.dev.BuildConfig;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.StandardExceptionParser;

import net.androidsea.seoultaxi.base.network.ServerConf;

import kr.swmaestro.sddp.iwillpass.util.NetworkUtil;


/**
 * 라이프 사이클만.
 * Created by rino0601 on 13. 10. 6..
 */
public abstract class GoogleAnalyticsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!BuildConfig.DEBUG) {
            EasyTracker.getInstance(this).activityStart(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!BuildConfig.DEBUG) {
            EasyTracker.getInstance(this).activityStop(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 사용자가 back key를 눌렀을 경우 호출되는 매서드.
     * 기본적으로 액티비티가 종료되도록 구현되어 있습니다.
     * 그리고 구글분석에 이벤트를 날리죠.
     * <p/>
     * 이 메서드를 오버라이드 한다면, 구글 분석 코드를 잊지 마시길...
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendUIEvent("onBackPressed()", "at " + this.getLocalClassName(), null);
    }

    public void sendException(Exception e) {
        if (!BuildConfig.DEBUG) {
            // May return null if EasyTracker has not yet been initialized with a
            // property ID.
            EasyTracker easyTracker = EasyTracker.getInstance(this);

            // StandardExceptionParser is provided to help get meaningful Exception descriptions.
            easyTracker.send(MapBuilder
                    .createException(new StandardExceptionParser(this, null)              // Context and optional collection of package names
                            // to be used in reporting the exception.
                            .getDescription(Thread.currentThread().getName(),    // The name of the thread on which the exception occurred.
                                    e),                                  // The exception.
                            false)                                               // False indicates a fatal exception
                    .build()
            );
        } else {
            Toast.makeText(this, "EXCEPTION:" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public static String EVENT_CATEGORY_UI_ACTION = "사용자 입력";

    public void sendUIEvent(String action, String label, Long value) {
        sendEvent(EVENT_CATEGORY_UI_ACTION, action, label, value);
    }

    private void sendEvent(String category, String action, String label, Long value) {
        if (!BuildConfig.DEBUG) {
            EasyTracker easyTracker = EasyTracker.getInstance(this);
            easyTracker.send(MapBuilder
                    .createEvent(category, // Event category (required)
                            action,        // Event action (required)
                            label,         // Event label
                            value)         // Event value
                    .build()
            );
        }
    }

    protected void initNetwork() {
        if (NetworkUtil.checkStatus(this) == NetworkUtil.NETWORK_NONE) {
            createNetworkDisabledAlert();
        }
        ServerConf.initialize(this.getApplicationContext(), "iwillpass", "IWillPass", "flaskapp-env-r2ynpptnff.elasticbeanstalk.com", 80);

        // # 여기 고치시면 됩니다.
    }

    protected void createNetworkDisabledAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("네트워크가 필요합니다.")
                .setCancelable(false)
                .setPositiveButton("환경설정 가기",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                                startActivity(gpsOptionsIntent);
                            }
                        })
                .setNegativeButton("닫 기",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
