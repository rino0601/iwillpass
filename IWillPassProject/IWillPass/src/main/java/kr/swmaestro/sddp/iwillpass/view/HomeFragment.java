package kr.swmaestro.sddp.iwillpass.view;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.HttpJsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by FlaShilver on 2013. 11. 11..
 */
public class HomeFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        ListView listview = (ListView) rootView.findViewById(R.id.home_list);
        ArrayList<String> lists = new ArrayList<String>();
        HomeAdapter homeAdapter = new HomeAdapter(getActivity(), R.layout.row_home, lists);
        listview.setAdapter(homeAdapter);

        lists.add("2014년 3월 1째주 시뮬 결과 입니다.\n참여 인원 : 3027명\n서울대 25명\n연세대 27명\n고려대 26명\n...\n기가스터디 614명\n\n *note하위 20%는 기가스터디에 가게 됩니다.");
        lists.add("여러분의 실제 수능을 진심으로 응원합니다!");

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user", "flashilver");
            jsonObject.put("subject", "korean");


            HttpJsonAPI.request("/api//v1.0/exam/1231/", "GET", jsonObject, new HttpJsonAPI.CallbackListener() {
                @Override
                public void callback(int error, JSONObject result) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rootView;

    }

    private class HomeAdapter extends ArrayAdapter<String> {

        public ArrayList<String> items;
        private Context mcontext;

        public HomeAdapter(Context context, int resource, ArrayList<String> items) {
            super(context, resource, items);
            mcontext = context;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.row_home, null);
            }

            String contents = items.get(position);

            if (contents != null) {
                TextView row_home_text = (TextView) v.findViewById(R.id.row_home_text);
                row_home_text.setText(contents);
            }

            return v;
        }
    }
}
