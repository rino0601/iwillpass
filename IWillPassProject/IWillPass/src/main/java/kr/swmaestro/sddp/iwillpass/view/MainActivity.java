package kr.swmaestro.sddp.iwillpass.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dulgi.MoGye.dev.BuildConfig;
import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.Error;
import net.androidsea.seoultaxi.base.api.HttpJsonAPI;

import org.json.JSONException;
import org.json.JSONObject;

import kr.swmaestro.sddp.iwillpass.model.Config;


/**
 * 스플래시 이미지 보여주는 액티비티.
 * 보여주는 사이, 마켓에 최신 버전이 있는지 체크 합니다.
 */
public class MainActivity extends GoogleAnalyticsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initNetwork();
        new VersionChecker().check();

        if (BuildConfig.DEBUG) {
            Toast.makeText(this, "디버그 모드 입니다", 1).show();
            //Todo 한종이 커밋이랑 충돌난 부분.
            //startActivity(new Intent(this, NaviMainActivity.class));

            startActivity(new Intent(this, NaviMainActivity.class));
            return;
        } else {
            Toast.makeText(this, "환영합니다", 1).show();
        }

    }

    private void goNext(boolean isInstance) {
        final Intent gotoOMR = new Intent(MainActivity.this, NaviMainActivity.class);
        if (isInstance) {
            startActivity(gotoOMR);
            return;
        }
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                startActivity(gotoOMR);
            }
        };
        handler.sendEmptyMessageDelayed(0, 100);  //원래 4000
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    private class VersionChecker implements HttpJsonAPI.CallbackListener {

        private final JSONObject requsetJsonParam;
        private int versionCodeFromClient;

        private VersionChecker() {
            requsetJsonParam = new JSONObject();
            try {
                requsetJsonParam.put("object", "/api/v1.0/config");
                requsetJsonParam.put("method", "GET");
            } catch (JSONException e) {
                e.printStackTrace();
                sendException(e);
            }
        }

        public void check() {
            HttpJsonAPI.request(requsetJsonParam, this);
        }

        @Override
        public void callback(int error, JSONObject result) {
            if (error != Error.SUCCESS) {
                sendException(new Exception("버전체크 실패."));
                goNext(false);
            }
            try {
                String market_url = result.getString("market_url");
                int active_user_count = result.getInt("active_user_count");
                Config.config.setActive_user_count(active_user_count);
                String apk_upgrade_message = result.getString("apk_upgrade_message");
                Log.d("resultres", apk_upgrade_message);
                Config.config.setApk_upgrade_message(apk_upgrade_message);
                int apk_version = result.getInt("apk_version");
                Config.config.setApk_version(apk_version);
                int target_user_count = result.getInt("target_user_count");
                Config.config.setTarget_user_count(target_user_count);
                String question_deadline = result.getString("question_deadline");
                Config.config.setQuestion_deadline(question_deadline);
                boolean apk_upgrade_force = result.getBoolean("apk_upgrade_force");
                Config.config.setApk_upgrade_force(apk_upgrade_force);


                if (isExistNewVersion(apk_version, apk_upgrade_message, market_url, apk_upgrade_force)) {
                    new InvitingGooglePlayDialog(MainActivity.this, apk_upgrade_message, market_url, versionCodeFromClient, apk_upgrade_force).show();
                } else {
                    goNext(false);
                }
            } catch (JSONException e) {
                sendException(e);
            }
        }

        private boolean isExistNewVersion(int version, String message, String market_url, boolean force) {
            versionCodeFromClient = 0;
            try {
                PackageManager packageManager = getPackageManager();
                if (packageManager != null) { // packgeManager를 못 불러 왔을때는... 그냥 어플 실행 시킬 수 밖에.
                    versionCodeFromClient = packageManager.getPackageInfo(getPackageName(), 0).versionCode;
                }
            } catch (PackageManager.NameNotFoundException e) {
                // Huh? Really?
                // doesn't make sense.
                e.printStackTrace(); // ignore exception.
            }
            return versionCodeFromClient < version;
        }
    }

    private class InvitingGooglePlayDialog extends Dialog implements View.OnClickListener {

        private String marketURL;
        private final int versionCodeFromClient;

        public InvitingGooglePlayDialog(Context context, String message, String marketURL, int versionCodeFromClient, boolean force) {
            super(context);
            this.marketURL = marketURL;
            this.versionCodeFromClient = versionCodeFromClient;
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.inviting_googleplay_dialog);

            TextView realeseNote = (TextView) findViewById(R.id.change_detail);
            realeseNote.setText(message);

            Button okButton = (Button) findViewById(R.id.okButton);
            okButton.setOnClickListener(this);

            Button cancelButton = (Button) findViewById(R.id.cancelButton);
            cancelButton.setVisibility(force ? View.GONE : View.VISIBLE);
            cancelButton.setOnClickListener(this);
            sendUIEvent("사용자가 업데이트 팝업을 만남", "v" + versionCodeFromClient, null);
        }

        @Override
        public void onClick(View v) { // google play로 보내버리고 어플 종료.
            switch (v.getId()) {
                case R.id.okButton: {
                    Intent marketLanunch = new Intent(Intent.ACTION_VIEW);
                    marketLanunch
                            .setData(Uri
                                    .parse(marketURL));
                    startActivity(marketLanunch);
                    finish();
                }
                break;
                case R.id.cancelButton: {
                    sendUIEvent("업데이트 팝업을 만났으나 업데이트 하지 않음", "v" + versionCodeFromClient, null);
                    goNext(true);
                }
                break;
            }
        }
    }
}
