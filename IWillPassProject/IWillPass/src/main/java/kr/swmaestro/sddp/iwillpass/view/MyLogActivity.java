package kr.swmaestro.sddp.iwillpass.view;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.dulgi.MoGye.dev.R;

/**
 * Created by rino0601 on 2013. 11. 6..
 */
public class MyLogActivity extends GoogleAnalyticsActivity {

    private WebView webView;
    public static final String ASSET_PATH = "file:///android_asset/";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylog);

        webView = (WebView) findViewById(R.id.webView);
//        webView.setWebViewClient(new WebViewClient());
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        webView.loadUrl(ASSET_PATH + "hello-pizza-chart.html");
    }

}