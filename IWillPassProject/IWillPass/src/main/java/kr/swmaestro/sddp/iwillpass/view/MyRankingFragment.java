package kr.swmaestro.sddp.iwillpass.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dulgi.MoGye.dev.R;
import java.util.ArrayList;

import kr.swmaestro.sddp.iwillpass.model.Config;
/**
 * Created by FlaShilver on 2013. 11. 12..
 */
public class MyRankingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_myranking, container, false);

        TextView second_text = (TextView) rootView.findViewById(R.id.second_text);
        TextView third_text = (TextView) rootView.findViewById(R.id.third_text);

        third_text.setText(Config.config.getActive_user_count()+"명/"+Config.config.getTarget_user_count()+"명\n("+Config.config.getActive_user_count() * 1.0/ Config.config.getTarget_user_count()*100.0+"%)");
//
//        Button kakao_btn = (Button) rootView.findViewById(R.id.kakao_button);
//        Button facebook_btn = (Button) rootView.findViewById(R.id.facebook_button);

        return rootView;
    }
}
