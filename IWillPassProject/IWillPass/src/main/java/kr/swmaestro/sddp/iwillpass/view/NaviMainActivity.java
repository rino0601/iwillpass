package kr.swmaestro.sddp.iwillpass.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dulgi.MoGye.dev.R;


/**
 * Created by FlaShilver on 2013. 11. 8..
 */
public class NaviMainActivity extends GoogleAnalyticsActivity {

    private String[] mDrawerTitles;       //복붙
    private DrawerLayout mDrawerLayout;   //복붙
    private ListView mDrawerList;//복붙
    private CharSequence mTitle;//복붙
    private CharSequence mDrawerTitle;//복붙
    private ActionBarDrawerToggle mDrawerToggle;//복붙

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navi_activity_main);

        initNetwork();

        Fragment fragment = null;    //복붙
        android.app.FragmentManager fragmentManager = getFragmentManager();   //복붙
        fragment = new DynamicTimerFragment();                         //복붙
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        mDrawerTitles = new String[]{"#지난주결과", "홈", "가상 입시", "입시 전적 보기", "내 랭킹 보기", "공부 타이머", "About us"}; //수정하세요.


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, mDrawerTitles);       //Navigation Drawer를 커스텀리스트뷰로 사용하실 예정이면 drawer_list_item의 xml이 바뀌어야합니다.

        mTitle = mDrawerTitle = getTitle();   //복붙

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);//복붙
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);   //복붙

        mDrawerList = (ListView) findViewById(R.id.left_drawer);  //복붙 (Left_drawer는 activity_main의 FrameLayout id입니다.)
        mDrawerList.setAdapter(arrayAdapter); //복붙
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());  //복붙


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) { //***복붙***
            public void onDrawerClosed(View view) {      //복붙
                getActionBar().setTitle(mTitle);   //복붙
            }

            public void onDrawerOpened(View drawerView) {   //복붙
                getActionBar().setTitle(mDrawerTitle);    //복붙
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);    //복붙
        //***복붙***
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState == null) {

        }
    }

    @Override //복붙 메소드
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override  //복붙 메소드
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override   //복붙 메소드
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override //***복붙 메소드***
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position); //몇번째 리스트를 클릭했는지 position을 통해서 받아온다.


        }
    }

    //***복붙 메소드***
    private void selectItem(int position) {
        Fragment fragment = null;     //새롭게 Fragment뷰를 띄울 준비.
        FragmentManager fragmentManager = getFragmentManager();

        switch (position) {
            case 0: //리스트에서 첫번째 아이템을 클릭했을때.
                fragment = new DynamicTimerFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case 1: //리스트에서 두번쨰 아이템을 클릭했을때.
                fragment = new HomeFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case 2: //리스트에서 세번쨰 아이템을 클릭했을때.
                fragment = new SimulationMenuFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case 3: //리스트에서 네번쨰 아이템을 클릭했을때.
                fragment = new AwordFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case 4: //리스트에서 다섯번쨰 아이템을 클릭했을때.
                fragment = new MyRankingFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case 5: //리스트에서 여섯번쨰 아이템을 클릭했을때.
                fragment = new StudyTimerFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            case 6: //리스트에서 아홉번쨰 아이템을 클릭했을때.
                fragment = new AboutusFragment();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                break;
            default:
                break;
        }
        mDrawerList.setItemChecked(position, true);     //복붙
        mDrawerLayout.closeDrawer(mDrawerList);     //복붙
    }

    //***복붙 메소드***
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
}
