package kr.swmaestro.sddp.iwillpass.view;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.dulgi.MoGye.dev.R;

import net.androidsea.seoultaxi.base.api.HttpJsonAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kr.swmaestro.sddp.iwillpass.SimulationExamActivity;
import kr.swmaestro.sddp.iwillpass.SimulationExplainActivity;
import kr.swmaestro.sddp.iwillpass.model.SimulationExplainData;
import kr.swmaestro.sddp.iwillpass.util.SystemInfo;

/**
 * Created by rino0601 on 2013. 11. 13..
 */
public class SimulationMenuFragment extends Fragment {

    private static final int SIMULATION_EXAM_ACTIVITY = 1;
    private Button examEnglish;
    private Button examMath;
    private Button reviewEnglish;
    private Button reviewMath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_simulation_menu, container, false);

        examEnglish = (Button) rootView.findViewById(R.id.buttonExamEnglish);
        examEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SimulationExamActivity.class);
                intent.putExtra(SimulationExamActivity.subjectKey, "english");
                startActivityForResult(intent, SIMULATION_EXAM_ACTIVITY);
            }
        });
        examMath = (Button) rootView.findViewById(R.id.buttonExamMath);
        examMath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SimulationExamActivity.class);
                intent.putExtra(SimulationExamActivity.subjectKey, "math");
                startActivityForResult(intent, SIMULATION_EXAM_ACTIVITY);
            }
        });

        reviewEnglish = (Button) rootView.findViewById(R.id.buttonReviewEnglish);
        reviewEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ReviewWaitDialog(getActivity(), R.id.buttonReviewEnglish).show();
            }
        });

        reviewMath = (Button) rootView.findViewById(R.id.buttonReviewMath);
        reviewMath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ReviewWaitDialog(getActivity(), R.id.buttonReviewMath).show();
            }
        });

//        reviewEnglish.setEnabled(false);
//        reviewMath.setEnabled(false);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIMULATION_EXAM_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                String subject = (String) extras.get(SimulationExamActivity.subjectKey);
                if (subject.equals("english")) {
//                    examEnglish.setEnabled(false);
//                    reviewEnglish.setEnabled(true);
                } else if (subject.equals("math")) {
//                    examMath.setEnabled(false);
//                    reviewMath.setEnabled(true);
                }
            }
        }
    }

    private class ReviewWaitDialog extends Dialog {

        private final Button nextButton;
        private final int buttonReviewId;
        private ArrayList<SimulationExplainData> intentData;

        private ReviewWaitDialog(Context context, int buttonReviewId) {
            super(context);
            this.buttonReviewId = buttonReviewId;
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_review_wait);
            nextButton = (Button) findViewById(R.id.button);
            nextButton.setText("서버에서 정보를 받아오는 중입니다.");
            nextButton.setEnabled(false);
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    Intent intent = new Intent(getActivity(), SimulationExplainActivity.class);
                    intent.putExtra(SimulationExplainActivity.dataArrayKey, intentData);
                    startActivity(intent);
                }
            });

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user", SystemInfo.getHashedPhoneNumber(context));
                jsonObject.put("subject", getSubjectString());
                String url = "/api/v1.0/exam/yesterday";
//                String url = "/api/v1.0/exam/today";
                HttpJsonAPI.request(url, "GET", jsonObject, new HttpJsonAPI.CallbackListener() {
                    @Override
                    public void callback(int error, JSONObject result) {
                        try {
                            JSONArray dataList = result.getJSONArray("data");
                            int dataCount = result.getInt("data_count");
                            intentData = new ArrayList<SimulationExplainData>(dataCount);
                            for (int i = 0; i < dataCount; i++) {
                                JSONObject data = dataList.getJSONObject(i);
                                String id = data.getString("id");
                                String answer = data.getString("answer");
                                String questionImageUrl = data.getString("question_image_url");
                                String answerImageUrl = data.getString("explain_image_url");
                                int errorRate = data.getInt("error_rate");
                                int point = data.getInt("point");
                                String type = data.getString("type");
                                intentData.add(new SimulationExplainData(id, answer, questionImageUrl, answerImageUrl, errorRate, point, type));
                            }

                            nextButton.setText("복습하기!");
                            nextButton.setEnabled(true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "데이터 다운로드 실패", Toast.LENGTH_SHORT).show();
                            dismiss();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private String getSubjectString() {
            switch (buttonReviewId) {
                case R.id.buttonReviewEnglish:
                    return "english";
                case R.id.buttonReviewMath:
                    return "math";
                default:
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public void onBackPressed() {
            // prevent back key;
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            Rect dialogBounds = new Rect();
            getWindow().getDecorView().getHitRect(dialogBounds);
            if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
                return false; // prevent outside touch dismiss
            }
            return super.dispatchTouchEvent(ev);
        }
    }
}