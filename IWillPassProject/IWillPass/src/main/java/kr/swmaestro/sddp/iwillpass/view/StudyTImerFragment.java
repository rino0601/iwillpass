package kr.swmaestro.sddp.iwillpass.view;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dulgi.MoGye.dev.R;

import java.util.Date;

import kr.swmaestro.sddp.iwillpass.model.StudyTimerModel;

/**
 * Created by FlaShilver on 2013. 11. 14..
 */
public class StudyTimerFragment extends Fragment {

    long difftime = 0;
    Date pick_timer;
    TextView timer_text;
    LinearLayout subject_select;
    ImageButton startnstop;
    ImageButton initalnsave;
    private Handler handler = new Handler();
    int selected_subject = 0;
    TextView subject_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_study_timer, container, false);


        subject_title = (TextView) rootView.findViewById(R.id.subject_title);
        subject_select = (LinearLayout) rootView.findViewById(R.id.subject_select);
        ImageView korean_btn = (ImageView) rootView.findViewById(R.id.korean_button);
        ImageView math_btn = (ImageView) rootView.findViewById(R.id.math_button);
        ImageView english_btn = (ImageView) rootView.findViewById(R.id.english_button);
        ImageView tamgu_btn = (ImageView) rootView.findViewById(R.id.tamgu_button);

        timer_text = (TextView) rootView.findViewById(R.id.studytimer);
        startnstop = (ImageButton) rootView.findViewById(R.id.startnstop);
        initalnsave = (ImageButton) rootView.findViewById(R.id.initalnsave);

        //Todo 국어는 1 수학은 2 외국어는 3 탐구는 4

        korean_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudyTimerModel.studyTimerModel.saveStudyTime(difftime);

                selected_subject = 1;
                StudyTimerModel.studyTimerModel.setNow_subject(selected_subject);
                select_subject(selected_subject);
            }
        });

        math_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudyTimerModel.studyTimerModel.saveStudyTime(difftime);

                selected_subject = 2;
                StudyTimerModel.studyTimerModel.setNow_subject(selected_subject);
                select_subject(selected_subject);

            }
        });

        english_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudyTimerModel.studyTimerModel.saveStudyTime(difftime);

                selected_subject = 3;
                StudyTimerModel.studyTimerModel.setNow_subject(selected_subject);
                select_subject(selected_subject);

            }
        });

        tamgu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudyTimerModel.studyTimerModel.saveStudyTime(difftime);

                selected_subject = 4;
                StudyTimerModel.studyTimerModel.setNow_subject(selected_subject);
                select_subject(selected_subject);

            }
        });

        startnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//타이머를 작동시키는..시작
                if (StudyTimerModel.studyTimerModel.isNow_working() == false) {
                    startnstop.setBackgroundResource(R.drawable.button_pause);
                    initalnsave.setBackgroundResource(R.drawable.button_save);
                    StudyTimerModel.studyTimerModel.setNow_working(true);
                    operateTimer();
                } else {//타이머를 멈추는..정지
                    startnstop.setBackgroundResource(R.drawable.button_start);
                    initalnsave.setBackgroundResource(R.drawable.button_reset);
                    StudyTimerModel.studyTimerModel.setNow_working(false);
                    stopTimer();
                }
            }
        });

        initalnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StudyTimerModel.studyTimerModel.isNow_working() == false) {//현재 과목의 공부시간을 초기화 시키는... 초기화
                    difftime = StudyTimerModel.studyTimerModel.initializeTimer(StudyTimerModel.studyTimerModel.getNow_subject());
                    StudyTimerModel.studyTimerModel.setSave_diff_time(difftime);
                    setTimertext(difftime);
                } else {//현재 과목의 공부시간을 저장 하는... 저장
                    StudyTimerModel.studyTimerModel.setNow_working(false);
                    startnstop.setBackgroundResource(R.drawable.button_start);
                    initalnsave.setBackgroundResource(R.drawable.button_reset);
                    StudyTimerModel.studyTimerModel.saveStudyTime(difftime);
                    stopTimer();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (StudyTimerModel.studyTimerModel.isNow_working() == true) {
            StudyTimerModel.studyTimerModel.setSave_diff_time(difftime);
            StudyTimerModel.studyTimerModel.setPick_timer(new Date());
            handler.removeCallbacks(doUpdateTimer);
        } else if (StudyTimerModel.studyTimerModel.isNow_working() == false) {
            stopTimer();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(StudyTimerModel.studyTimerModel.getNow_subject()==1){
            subject_title.setText("국어");
        }else if(StudyTimerModel.studyTimerModel.getNow_subject()==2){
            subject_title.setText("수학");
        }else if(StudyTimerModel.studyTimerModel.getNow_subject()==3){
            subject_title.setText("영어");
        }else if(StudyTimerModel.studyTimerModel.getNow_subject()==4){
            subject_title.setText("탐구");
        }

        if (StudyTimerModel.studyTimerModel.isNow_working() == false) {
            subject_select.setVisibility(View.VISIBLE);
            setTimertext(StudyTimerModel.studyTimerModel.getSave_diff_time());
            startnstop.setBackgroundResource(R.drawable.button_start);
            initalnsave.setBackgroundResource(R.drawable.button_reset);
        } else if (StudyTimerModel.studyTimerModel.isNow_working() == true) {
            pick_timer = StudyTimerModel.studyTimerModel.getPick_timer();
            subject_select.setVisibility(View.INVISIBLE);
            startnstop.setBackgroundResource(R.drawable.button_pause);
            initalnsave.setBackgroundResource(R.drawable.button_save);
            handler.postDelayed(doUpdateTimer, 1000);
        }
    }

    private void select_subject(int selected_subject) {
        if (selected_subject == 1) {
            StudyTimerModel.studyTimerModel.setSave_diff_time(StudyTimerModel.studyTimerModel.getKorean_studytime());
            difftime = StudyTimerModel.studyTimerModel.getSave_diff_time();
            setTimertext(difftime*1000);
            StudyTimerModel.studyTimerModel.setSave_diff_time(difftime*1000);

            subject_title.setText("국어");

        } else if (selected_subject == 2) {
            StudyTimerModel.studyTimerModel.setSave_diff_time(StudyTimerModel.studyTimerModel.getMath_studytime());
            difftime = StudyTimerModel.studyTimerModel.getSave_diff_time();

            setTimertext(difftime*1000);  //현재의 save_diff_time을 가져옴.
            StudyTimerModel.studyTimerModel.setSave_diff_time(difftime*1000);
            subject_title.setText("수학");

        } else if (selected_subject == 3) {
            StudyTimerModel.studyTimerModel.setSave_diff_time(StudyTimerModel.studyTimerModel.getEnglish_studytime());
            difftime = StudyTimerModel.studyTimerModel.getSave_diff_time();
            setTimertext(difftime*1000);
            StudyTimerModel.studyTimerModel.setSave_diff_time(difftime*1000);


            subject_title.setText("영어");

        } else if (selected_subject == 4) {
            StudyTimerModel.studyTimerModel.setSave_diff_time(StudyTimerModel.studyTimerModel.getTamgu_studytime());
            difftime = StudyTimerModel.studyTimerModel.getSave_diff_time();
            setTimertext(difftime*1000);
            StudyTimerModel.studyTimerModel.setSave_diff_time(difftime * 1000);

            subject_title.setText("탐구");

        }

    }


    private void stopTimer() {
        StudyTimerModel.studyTimerModel.setSave_diff_time(difftime);
        subject_select.setVisibility(View.VISIBLE);
        handler.removeCallbacks(doUpdateTimer);
    }

    private void operateTimer() {
        pick_timer = new Date();
        subject_select.setVisibility(View.INVISIBLE);
        handler.postDelayed(doUpdateTimer, 1000);
    }

    public void setTimertext(long difftime) {
        timer_text.setText((difftime / 1000 / 60 / 60) + "시간 " + (difftime / 1000 / 60) + "분 " + (difftime / 1000 % 60) + "초");
    }

    public Runnable doUpdateTimer = new Runnable() {
        public void run() {
            Date now_timer = new Date();
            difftime = StudyTimerModel.studyTimerModel.getSave_diff_time() + ((now_timer.getTime() - pick_timer.getTime()));

            setTimertext(difftime);

            handler.postDelayed(doUpdateTimer, 1000);
        }
    };
}