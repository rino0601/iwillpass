package kr.swmaestro.sddp.iwillpass.view;

/**
 * Created by FlaShilver on 2013. 10. 18..
 */

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

import com.dulgi.MoGye.dev.R;

import kr.swmaestro.sddp.iwillpass.activityview.AboutusActivity;
import kr.swmaestro.sddp.iwillpass.activityview.CalculatorActivity;
import kr.swmaestro.sddp.iwillpass.activityview.CommunityActivity;
import kr.swmaestro.sddp.iwillpass.activityview.DynamicTimerActivity;
import kr.swmaestro.sddp.iwillpass.activityview.ExamListActivity;
import kr.swmaestro.sddp.iwillpass.activityview.TimerActivity;

public class TabMainActivity extends TabActivity implements OnTabChangeListener {
    TabHost mTabHost;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabmain);
        // ÅÇ ÁöÁ¤ ½ÃÀÛ

        Resources res = getResources();

       /*
        View titleView = getWindow().findViewById(android.R.id.title);
        ViewParent parent = titleView.getParent();
        View parentView = (View) parent;
        parentView.setBackgroundColor(Color.rgb(0, 175, 255));
        */


        mTabHost = getTabHost();
        mTabHost.setOnTabChangedListener(this);

        // .setIndicator("", res.getDrawable(R.drawable.dday_tab))

        mTabHost.addTab(mTabHost.newTabSpec("tab_test1")
                .setIndicator("", res.getDrawable(R.drawable.selector_tab1))
                .setContent(new Intent(this, DynamicTimerActivity.class)));

        mTabHost.addTab(mTabHost.newTabSpec("tab_test2")
                .setIndicator("", res.getDrawable(R.drawable.selector_tab2))
                .setContent(new Intent(this, ExamListActivity.class)));

        mTabHost.addTab(mTabHost.newTabSpec("tab_test3")
                .setIndicator("", res.getDrawable(R.drawable.selector_tab3))
                .setContent(new Intent(this, TimerActivity.class)));

        mTabHost.addTab(mTabHost.newTabSpec("tab_test4")
                .setIndicator("", res.getDrawable(R.drawable.selector_tab4))
                .setContent(new Intent(this, CalculatorActivity.class)));

        mTabHost.addTab(mTabHost.newTabSpec("tab_test5")
                .setIndicator("", res.getDrawable(R.drawable.selector_tab5))
                .setContent(new Intent(this, AboutusActivity.class)));

        // Tab¿¡ »ö ÁöÁ¤
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            mTabHost.getTabWidget().getChildAt(i)
                    .setBackgroundColor(Color.parseColor("#000000"));
        }
        mTabHost.getTabWidget().setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0)
                .setBackgroundColor(Color.parseColor("#00a2ff"));
    }

    public void onTabChanged(String tabId) {
        // Titlebar ³»¿ë º¯°æ
        // Tab »ö º¯°æ
        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            mTabHost.getTabWidget().getChildAt(i)
                    .setBackgroundColor(Color.parseColor("#000000"));
        }
        mTabHost.getTabWidget().getChildAt(mTabHost.getCurrentTab())
                .setBackgroundColor(Color.parseColor("#00a2ff"));

    }
}
