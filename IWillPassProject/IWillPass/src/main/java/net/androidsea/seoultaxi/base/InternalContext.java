package net.androidsea.seoultaxi.base;

import android.content.Context;
import android.os.Handler;

import net.androidsea.seoultaxi.base.network.ServerConf;
import net.androidsea.seoultaxi.base.util.GMLog;

/**
 * 내부 데이터를 보관하는 객체, 외부에 노출되지 않는다.
 */
public class InternalContext {

    private static final String TAG = InternalContext.class.getName();
    private static InternalContext mInstance = null;
    private boolean mInitialized = false;
    private Context mApplicationContext = null;
    private String mApplicationId = null;
    private String mApplicationName = null;
    private Handler mHandler = null;
    private ServerConf mServerConf = new ServerConf();

    public static InternalContext getInstance() {
        if (mInstance == null) {
            mInstance = new InternalContext();
        }
        return mInstance;
    }

    public ServerConf getServerConf() {
        return mServerConf;
    }

    public Handler getHandler() {
        return mHandler;
    }

    public boolean isInitialized() {
        return mInitialized;
    }

    public void initialize(Context applicationContext, String applicationId, String applicationName) {

        GMLog.i(TAG, "initialize");

        if (mInitialized) {
            GMLog.e(TAG, "Already initialized");
            return;
        }

        // main thread 여부 판단
        Thread ct = Thread.currentThread();
        GMLog.i(TAG, "current thread name " + ct.getName());
        if (!"main".equals(ct.getName())) {
            GMLog.e(TAG, "current thread is not main");
            return;
        }

        // 초기화 실패하더라도 Main Thread Handler 는 존재해야 내부 Thread 동작이 정상적이며, 외부로 결과를 알릴
        // 수 있다.
        mHandler = new Handler();

        // 입력 패러미터 유효성 판단
        GMLog.i(TAG, "applicationContext : " + applicationContext);
        GMLog.i(TAG, "applicationId : " + applicationId);
        GMLog.i(TAG, "applicationName : " + applicationName);

        if (applicationContext == null) {
            GMLog.e(TAG, "applicationContext is invalid");
            return;
        }
        if (applicationId == null || applicationId.length() == 0) {
            GMLog.e(TAG, "applicationId is invalid");
            return;
        }
        if (applicationName == null || applicationName.length() == 0) {
            GMLog.e(TAG, "applicationName is invalid");
            return;
        }

        mApplicationContext = applicationContext;
        mApplicationId = applicationId;
        mApplicationName = applicationName;

        // 초기화 여부
        mInitialized = true;


        GMLog.i(TAG, "initialize return");
    }

}
