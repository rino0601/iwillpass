package net.androidsea.seoultaxi.base.api;

/**
 * 오류 정의 클래스
 */
public abstract class Error {

    // 공용
    /**
     * 성공
     */
    public static final int SUCCESS = 0;
    /**
     * 오류, 정의되지 않음
     */
    public static final int UNKNOWN = 1;
    /**
     * 취소
     */
    public static final int CANCELED = 2;


    // 클라이언트 오류 2자리
    /**
     * 라이브러리가 초기화되지 않음
     */
    public static final int CLIENT_NOT_INITIALIZE = 11;

    /**
     * 입력 패러미터가 잘못됨
     */
    public static final int CLIENT_INAVLID_PARAMETER = 12;

    /**
     * 서버 응답을 이해할 수 없음
     */
    public static final int CLIENT_CAN_NOT_PARSE_SERVER_RESPONSE = 13;

    /**
     * 네트워크를 사용할 수 없음
     */
    public static final int CLIENT_NOT_READY_NETWORK = 14;

    /**
     * SD 카드에 접근할 수 없음. SD 카드가 없거나 마운트되지 않음, 용량 부족
     */
    public static final int CLIENT_NOT_READY_SDCARD = 15;

    /**
     * UI Activity 로 전달된 화면(페이지)가 존재하지 않음
     */
    public static final int CLIENT_NOT_EXIST_PAGE = 16;

    /**
     * 사용자가 다른 게임을 실행하여 UI Activity 종료됨
     */
    public static final int CLIENT_RUN_ANOTHER_GAME = 17;

    /**
     * 이미 최신버전으로 update 된 경우의 리턴됨. 리소스 업데이트 할 필요 없음.
     */
    public static final int CLIENT_ALREADY_UPDATED = 18;

    /**
     * 리소스 업데이트 중, 서버로부터 압축파일 다운로드 하던 중 실패한 경우 리턴한다.
     */
    public static final int CLIENT_RESOURCE_DOWNLOAD_FAILED = 19;

    /**
     * 리소스 업데이트 중, 압축 해제를 하다 실패한 경우 리턴한다.
     */
    public static final int CLIENT_RESOURCE_UNZIP_FAILED = 20;

    /**
     * SDCard 용량이 부족한 경우
     */
    public static final int CLIENT_SDCARD_INSUFFICIENT = 21;

    /**
     * 알 수 없는 이유로 웹을 실행할 수 없는 상태
     */
    public static final int CLIENT_NOT_ENABLE_TO_START = 22;

    // 통신 오류 3자리
    // 별도 정의 안함. HttpURLConnection.HTTP_OK 형태의 HTTP Status Code 사용

    // 서버 기능 오류 4자리, 서버 API Error Code 사용
    /**
     * 가입하지 않은 사용자
     */
    public static final int SERVER_AUTH_NOT_MEMBER = 1301;

    /**
     * 서버 점검중
     */
    public static final int SERVER_INSPECTION = 9001;

    private Error() {
    }
}
