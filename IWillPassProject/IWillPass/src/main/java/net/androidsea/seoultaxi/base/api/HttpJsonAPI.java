package net.androidsea.seoultaxi.base.api;

import net.androidsea.seoultaxi.base.InternalContext;
import net.androidsea.seoultaxi.base.network.RequestHandler;
import net.androidsea.seoultaxi.base.network.ResponseListener;
import net.androidsea.seoultaxi.base.util.GMLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class HttpJsonAPI {

    public interface CallbackListener {
        /**
         * @param error
         * @param result {"error":0,"result":{json_object}}의 꼴을 가지는 서버response의 json_object!
         */
        public void callback(int error, JSONObject result);
    }

    private static final String TAG = HttpJsonAPI.class.getName();

    public static String request(String url, String METHOD, final JSONObject argument, final CallbackListener listener) throws JSONException {
        JSONObject param = new JSONObject();
        param.put("object", url);
        param.put("method", METHOD);
        if (argument != null)
            param.put("argument", argument);
        return request(param, listener);
    }

    /**
     * @param param    ex)
     *                 JSONObject param = new JSONObject();
     *                 try {
     *                 JSONObject argument = new JSONObject();
     *                 argument.put("user", user);
     *                 <p/>
     *                 param.put("object", "/taxi/region_seoul");
     *                 param.put("method", "GET");
     *                 param.put("argument", argument);
     *                 } catch (JSONException e) {
     *                 GMLog.e(TAG, "request: JSONException at parameter input");
     *                 }
     * @param listener
     * @return
     */
    public static String request(final JSONObject param, final CallbackListener listener) {
        ResponseListener responseListener = new ResponseListener() {
            @Override
            public void onResponse(String object, String requestId, final int status, final String response) {
                // 초기화 여부 검사
                if (!InternalContext.getInstance().isInitialized()) {
                    listener.callback(Error.CLIENT_NOT_INITIALIZE, null);
                    return;
                }
                // 통신 에러 검사
                if (status != HttpURLConnection.HTTP_OK) {
                    listener.callback(status, null);
                    return;
                }
                Runnable responseRunnable = new DownLoadResponseRunnable(listener, response);
                InternalContext.getInstance().getHandler().post(responseRunnable);
            } // end of onResponse
        };
        String requestId = RequestHandler.getInstance().request(param, responseListener);

        return requestId;
    }

    public static void cancel(String requestId) {
        GMLog.i(TAG, "cancel");

        if (requestId == null) {
            GMLog.e(TAG, "In cancel, requestId param is null");
            return;
        }
        RequestHandler.getInstance().cancelRequest(requestId);
    }

    private static class DownLoadResponseRunnable implements Runnable {
        private CallbackListener listener;
        private String response;

        public DownLoadResponseRunnable(CallbackListener listener, String response) {
            this.listener = listener;
            this.response = response;
        }

        @Override
        public void run() {
            try {
                JSONObject resp = new JSONObject(response);
                if (isFunctionErrorOccur(resp)) {
                    return;
                }
                JSONObject result = resp.getJSONObject("result");
                listener.callback(Error.SUCCESS, result);
            } catch (JSONException e) {
                // 파싱 에러
                listener.callback(Error.CLIENT_CAN_NOT_PARSE_SERVER_RESPONSE, null);
            }
        }


        private boolean isFunctionErrorOccur(JSONObject resp) throws JSONException {
            // 기능 에러 검사
            if (resp.has("error")){
                int error = resp.getInt("error");
                if (error != Error.SUCCESS) {
                    listener.callback(error, null);
                    return true;
                }
            }
            return false;
        }
    }
}

