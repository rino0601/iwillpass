package net.androidsea.seoultaxi.base.image;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.dulgi.MoGye.dev.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;
import java.lang.ref.WeakReference;

public class ImageDownloader extends AsyncTask {
    private final Context context;

    private ProgressDialog progDailog;

    private class ImageInfo {
        private String url;
        private Bitmap bitmap;

        public ImageInfo(String url, Bitmap bitmap) {
            this.setBitmap(bitmap);
            this.setUrl(url);
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public String getUrl() {
            return url;
        }

        public void setBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    private final WeakReference imageViewReference;

    public ImageDownloader(Context context, ImageView imageView) {
        this.context = context;
        imageViewReference = new WeakReference(imageView);
    }

    @Override
    protected Object doInBackground(Object[] params) {
        return downloadBitmap(params[0].toString());
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progDailog = new ProgressDialog(context);
        progDailog.setMessage("Loading...");
        progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDailog.setCancelable(true);
        progDailog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        ImageInfo imageInfo = (ImageInfo) o;
        if (imageInfo != null) {
            Log.i("Async-Example", "onPostExecute Called URL:" + imageInfo.getUrl());
            //1. 이미지 출력
            Bitmap bitmap = imageInfo.getBitmap();
            String url = imageInfo.getUrl();
            if (imageViewReference != null) {
                ImageView imageView = (ImageView) imageViewReference.get();
                if (imageView != null) {
                    if (bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                    } else {
                        imageView.setImageResource(R.drawable.ic_launcher);
                    }
                }
            }
            //2. 이미지 로컬 저장
        } else {
            Log.i("Async-Example", "onPostExecute Called URL:NULL");
        }
        progDailog.dismiss();
    }

    private ImageInfo downloadBitmap(String url) {
        // initilize the default HTTP client object
        final DefaultHttpClient client = new DefaultHttpClient();

        //forming a HttoGet request
        final HttpGet getRequest = new HttpGet(url);
        try {

            HttpResponse response = client.execute(getRequest);

            //check 200 OK for success
            final int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode +
                        " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    // getting contents from the stream
                    inputStream = entity.getContent();

                    // decoding stream data back into image Bitmap that android understands
                    final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    ImageInfo imageInfo = new ImageInfo(url, bitmap);
                    return imageInfo;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // You Could provide a more explicit error message for IOException
            getRequest.abort();
            Log.e("ImageDownloader", "Something went wrong while" +
                    " retrieving bitmap from " + url + e.toString());
        }
        return null;
    }
}
