package net.androidsea.seoultaxi.base.network;

import net.androidsea.seoultaxi.base.InternalContext;
import net.androidsea.seoultaxi.base.escape.PercentEscaper;
import net.androidsea.seoultaxi.base.util.GMLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

//import javax.net.ssl.HostnameVerifier;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLSession;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;

//import com.gminute.internal.InternalContext;
//import com.gminute.internal.util.GMLog;
//import com.gminute.internal.vendor.com.google.api.client.escape.PercentEscaper;


public abstract class BaseRequest {
    private static final String TAG = BaseRequest.class.getName();

    private static int DEFAULT_RETRIES = 1;
    protected static int DEFAULT_TIMEOUT = 30 * 1000;
    protected static String DEFAULT_ENCODING = "UTF-8";

    protected String mMethod;
    protected JSONObject mParams;

    protected int mResponseCode = 600;
    protected String mResponseBody = null;
    protected String mResponseType = null;

    public BaseRequest() {
        super();
    }

    public BaseRequest(JSONObject args) {
        super();
        setArgs(args);
    }

    public abstract String path();

    public int numRetries() {
        return DEFAULT_RETRIES;
    }

    public long timeout() {
        return DEFAULT_TIMEOUT;
    }

    public String method() {
        return mMethod;
    }

    protected String getResponseType() {
        return mResponseType;
    }

    public int getResponseCode() {
        return mResponseCode;
    }

    public String getResponseBody() {
        return null == mResponseBody ? "" : mResponseBody;
    }

    public String url() {
        ServerConf serverConf = InternalContext.getInstance().getServerConf();
        return "http://" + serverConf.getHost() + ":" + serverConf.getPort() + path();
    }

    public final void setArgs(JSONObject params) {
        if (mParams == null)
            mParams = new JSONObject();
        mParams = params;
    }

    protected String getArgString() {
        StringBuilder accumulator = null;
        PercentEscaper escaper = new PercentEscaper(PercentEscaper.SAFECHARS_URLENCODER, true);

//		if(Auth.getInstance().isLoggedIn() == true) {
//			accumulator = new StringBuilder();
//			// String token = escaper.escape(Signer.getInstance().getAccessToken());
//			String token = escaper.escape(Auth.getInstance().getAccessToken());
//			accumulator.append("access_token=").append(token);
//		}

        if (mParams == null)
            return null == accumulator ? null : accumulator.toString();

        Iterator<String> keys = mParams.keys();

        String key = null;
        while (keys.hasNext()) {

            if (accumulator == null) {
                accumulator = new StringBuilder();
            } else {
                accumulator.append('&');
            }

            key = keys.next();
            accumulator.append(escaper.escape(key));
            accumulator.append('=');
            try {
                if (mParams.getString(key) != null) {
                    accumulator.append(escaper.escape(mParams.getString(key)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return accumulator == null ? null : accumulator.toString();
    }

//	private void trustAllHosts() {
//		// Create a trust manager that does not validate certificate chains
//		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//				return new java.security.cert.X509Certificate[] {};
//			}
//			@Override
//			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
//					throws java.security.cert.CertificateException {
//			}
//			@Override
//			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
//					throws java.security.cert.CertificateException {
//			}
//		} };
//
//		// Install the all-trusting trust manager
//		try {
//			SSLContext context = SSLContext.getInstance("TLS");
//			context.init(null, trustAllCerts, new java.security.SecureRandom());
//			HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

//	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
//		@Override
//		public boolean verify(String hostname, SSLSession session) {
//			return true;
//		}
//	};

    public void exec() {
        GMLog.i(TAG, "exec");

        if (InternalContext.getInstance().isInitialized() == false) {
            return;
        }

		/*
        int longtime = 0;
		for(int consume = 0; consume < (Integer.MAX_VALUE>>2); consume++) {
			longtime = consume;
		}
		longtime++;
		*/

        String meth = method();
        URL url = null;
//		HttpsURLConnection conn = null;
        HttpURLConnection conn = null;
        String args = getArgString();

        if (args != null)
            GMLog.i(TAG, "arguments : " + args);

//		trustAllHosts();
        System.setProperty("http.keepAlive", "false");

        try {
            if (meth.equals("GET") || meth.equals("DELETE")) {
                StringBuilder uri = new StringBuilder(url());
                if (args != null)
                    uri.append("?").append(args);

                url = new URL(uri.toString());
//				conn = makeHttpsConnection(url);
                conn = makeHttpConnection(url);
                if (null == conn)
                    throw new Exception();

                conn.setRequestMethod(meth.toUpperCase());
                conn.setRequestProperty("Content-Type", "application/json; charset=" + DEFAULT_ENCODING);
            } else if (meth.equals("POST") || meth.equals("PUT")) {

                url = new URL(url());
//				conn = makeHttpsConnection(url);
                conn = makeHttpConnection(url);
                if (null == conn)
                    throw new Exception();

                conn.setRequestMethod(meth.toUpperCase());
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=" + DEFAULT_ENCODING);

                conn.setDoOutput(true);

                if (null != args) {
                    OutputStream opstrm = conn.getOutputStream();
                    opstrm.write(args.getBytes(DEFAULT_ENCODING));
                    opstrm.flush();
                    opstrm.close();
                }
            }

            String buffer = null;
            StringBuilder response = new StringBuilder();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), DEFAULT_ENCODING));
            while ((buffer = in.readLine()) != null) {
                response.append(buffer);
            }
            in.close();

            mResponseCode = conn.getResponseCode();
            mResponseType = conn.getContentType();
            mResponseBody = response.toString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != conn) {
                try {
                    mResponseCode = conn.getResponseCode();
                    if (mResponseCode <= 0) { //혹시나 모를 상황에 대비
                        mResponseCode = 601;
                    }
                } catch (IOException e) {
                    GMLog.e(TAG, e.getMessage());
                }
                conn.disconnect();
            }
        }

        // for debug
        GMLog.i(TAG, meth + " " + (null != url ? url.toString() : "url is null"));
        GMLog.i(TAG, "status code : " + mResponseCode);
        GMLog.i(TAG, null != mResponseBody ? mResponseBody : "response body is null");
        GMLog.i(TAG, "exec done");
    }

    private HttpURLConnection makeHttpConnection(URL url) throws IOException {
        if (null == url)
            return null;
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        return conn;
    }

//	private HttpsURLConnection makeHttpsConnection(URL url) throws IOException {
//		if(null == url)
//			return null;
//
//		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//
//		conn.setHostnameVerifier(DO_NOT_VERIFY);
//		conn.setConnectTimeout(DEFAULT_TIMEOUT);
//		conn.setReadTimeout(DEFAULT_TIMEOUT);
//
//		conn.setRequestProperty("Charset", DEFAULT_ENCODING);
//
//		conn.setDoInput(true);
//		conn.setUseCaches(false);
//		conn.setDefaultUseCaches(false);
//
//		return conn;
//	}
}
