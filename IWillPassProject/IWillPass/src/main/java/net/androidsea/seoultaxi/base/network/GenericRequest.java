package net.androidsea.seoultaxi.base.network;

import org.json.JSONObject;


public class GenericRequest extends JSONContentRequest {
    final String mPath;

    public GenericRequest(final String path, final String method,
                          final JSONObject args) {
        super();

        setArgs(args);

        mPath = path;
        mMethod = method.toUpperCase();
    }

    @Override
    public String path() {
        return mPath;
    }
}
