package net.androidsea.seoultaxi.base.network;

import org.json.JSONObject;

public abstract class JSONContentRequest extends BaseRequest {

    private static final String CONTENT_TYPE = "application/json";
    public static final String DESIRED_RESPONSE_PREFIX = CONTENT_TYPE + ";";

    public JSONContentRequest() {
        super();
    }

    protected boolean isResponseJSON() {
        // [TODO] 서버에서 response type이 무조건 text/html로 오고 있다..
//		String responseType = getResponseType();
//		return responseType != null && responseType.startsWith(DESIRED_RESPONSE_PREFIX);
        return true;
    }

    public JSONContentRequest(JSONObject args) {
        super(args);
    }

    @Override
    public String getResponseBody() {
        //isResponseJSON()
        return null == mResponseBody ? "{}" : mResponseBody;
    }
}