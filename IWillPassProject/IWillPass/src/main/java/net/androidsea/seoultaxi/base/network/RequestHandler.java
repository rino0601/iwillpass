package net.androidsea.seoultaxi.base.network;

import net.androidsea.seoultaxi.base.util.GMLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RequestHandler {
    private static final int MIN_THREADS = 2;
    private static final int MAX_THREADS = 4;
    private static final int EXCESS_THREAD_LIFETIME = 10;

    private static final int REQEUST_LIST_CMD_ADD = 1;
    private static final int REQEUST_LIST_CMD_REMOVE = 2;

    private static final String TAG = "RequestHandler";
    private static RequestHandler mInstance = null;
    final ExecutorService mExecutor;

    private HashMap<String, Worker> mRequestList = null;

    public static RequestHandler getInstance() {
        if (mInstance == null)
            mInstance = new RequestHandler();
        return mInstance;
    }

    private final class Executor extends ThreadPoolExecutor {
        Executor() {
            super(MIN_THREADS, MAX_THREADS, EXCESS_THREAD_LIFETIME, TimeUnit.SECONDS,
                    new LinkedBlockingQueue<Runnable>(), new RejectedExecutionHandler() {
                @Override
                public void rejectedExecution(Runnable arg0, ThreadPoolExecutor arg1) {
                    GMLog.e(TAG, "Can't submit runnable " + arg0.toString());
                }
            });
        }
    }

    private RequestHandler() {
        mExecutor = new Executor();
        mRequestList = new HashMap<String, Worker>();
    }

    synchronized private void updateRequestList(int command, String requestId, Worker worker) {
        if (mRequestList == null)
            return;

        switch (command) {
            case REQEUST_LIST_CMD_ADD:
                mRequestList.put(requestId, worker);
                break;
            case REQEUST_LIST_CMD_REMOVE:
                mRequestList.remove(requestId);
                break;
            default:
        }
    }

    public String request(JSONObject param, ResponseListener listener) {

        Worker worker = new Worker();
        worker.setResponseListener(listener);

        try {
            JSONObject jsonArgs = null;
            if (true == param.has("argument"))
                jsonArgs = param.getJSONObject("argument");
            else
                jsonArgs = new JSONObject();

            GenericRequest req = new GenericRequest(param.getString("object"), param.getString("method"), jsonArgs);
            worker.setRequest(req);
            enqueueRequest(worker);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return worker.getId();
    }


//	/**
//	 * 컨텐츠 서버로의 requesst
//	 */
//	public String contentRequest(JSONObject param, ResponseListener listener) {
//
//		Worker worker = new Worker();
//		worker.setResponseListener(listener);
//
//		try {
//			JSONObject jsonArgs = null;
//			if (true == param.has("argument"))
//				jsonArgs = param.getJSONObject("argument");
//			else
//				jsonArgs = new JSONObject();
//
//			ByteRequest req = new ByteRequest(param.getString("object"), param.getString("method"), jsonArgs,
//					param.getString("filePath"));
//
//			worker.setRequest(req);
//			enqueueRequest(worker);
//
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//
//		return worker.getId();
//
//	}

    private void enqueueRequest(final Worker worker) {
        GMLog.i(TAG, "enqueueRequest:" + worker.getId());

        final Runnable requestRunnable = new Runnable() {
            @Override
            public void run() {
                GMLog.i(TAG, "requestRunnable run");

                if (worker.isCanceled()) {
                    GMLog.e(TAG, "requestRunnable canceled");
                    updateRequestList(REQEUST_LIST_CMD_REMOVE, worker.getId(), null);
                    GMLog.i(TAG, "requestRunnable done");
                    return;
                }

                worker.getRequest().exec();

                if (worker.isCanceled() /* || worker.getFuture().isCancelled() */) {
                    GMLog.e(TAG, "requestRunnable canceled");
                } else {
                    worker.fireResponse();
                }

                updateRequestList(REQEUST_LIST_CMD_REMOVE, worker.getId(), null);

                GMLog.i(TAG, "requestRunnable done");
            }
        };

        worker.setFuture(mExecutor.submit(requestRunnable));

        updateRequestList(REQEUST_LIST_CMD_ADD, worker.getId(), worker);
    }

    public void cancelRequest(String requestId) {
        GMLog.i(TAG, "cancelRequest:" + requestId);

        if (mRequestList == null)
            return;

        Worker worker = mRequestList.get(requestId);
        if (worker != null) {
            worker.getFuture().cancel(true);
            worker.setIsCanceled(true);
            updateRequestList(REQEUST_LIST_CMD_REMOVE, worker.getId(), null);
        } else {
            GMLog.e(TAG, "request id not found.");
        }
    }

    public boolean isEmpty() {
        if (mRequestList.size() == 0)
            return true;
        return false;
    }
}
