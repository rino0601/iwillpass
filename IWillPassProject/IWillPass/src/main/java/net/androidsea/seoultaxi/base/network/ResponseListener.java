package net.androidsea.seoultaxi.base.network;

public interface ResponseListener {
    public void onResponse(String object, String requestId, final int status, final String response);
}
