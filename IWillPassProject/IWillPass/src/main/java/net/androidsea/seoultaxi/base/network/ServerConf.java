package net.androidsea.seoultaxi.base.network;

import android.content.Context;

import net.androidsea.seoultaxi.base.InternalContext;

import java.util.ArrayList;

/**
 * 서버 설정
 */
public class ServerConf {

    /**
     * 상용 서버
     */
    public static final int SERVER_SERVICE = 0;
    /**
     * 개발자 서버
     */
    public static final int SERVER_DEV = 1;
    private static String ip;
    private static int port;

    /**
     * 초기화, 반드시 메인 쓰레드에서 호출해야 한다.
     *
     * @param applicationContext application context
     * @param applicationId      서버에 등록된 애플리케이션 Id
     * @param applicationName    애플리케이션 이름
     */
    public static void initialize(Context applicationContext, String applicationId, String applicationName, String ip, int port) {
        ServerConf.ip = ip;
        ServerConf.port = port;
        InternalContext.getInstance().initialize(applicationContext, applicationId, applicationName);
    }

    //TODO 내부 개발시 아래 서버 타입을 수정하여 사용 할 것
    //TODO 릴리즈시 BP 서버 or SERVICE 타입으로 셋팅하여 배포 할 것
    private int mType = ServerConf.SERVER_SERVICE;
    private ArrayList<ServerItem> mList = new ArrayList<ServerItem>();

    private class ServerItem {
        public String host;
        public int port;
        public String contentHost;
        public int contentPort;

        public ServerItem(String host, int port, String contentHost, int contentPort) {
            this.host = host;
            this.port = port;
            this.contentHost = contentHost;
            this.contentPort = contentPort;
        }
    }

    public ServerConf() {
        mList.add(ServerConf.SERVER_SERVICE, new ServerItem(ServerConf.ip, ServerConf.port, ServerConf.ip, ServerConf.port));
        mList.add(ServerConf.SERVER_DEV, new ServerItem(ServerConf.ip, ServerConf.port, ServerConf.ip, ServerConf.port));
    }

    public int getType() {
        return mType;
    }

    public String getHost() {
        return mList.get(mType).host;
    }

    public int getPort() {
        return mList.get(mType).port;
    }

    public String getContentHost() {
        return mList.get(mType).contentHost;
    }

    public int getContentPort() {
        return mList.get(mType).contentPort;
    }
}
