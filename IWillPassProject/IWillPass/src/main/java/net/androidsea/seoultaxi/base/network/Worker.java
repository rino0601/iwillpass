package net.androidsea.seoultaxi.base.network;

import net.androidsea.seoultaxi.base.util.GMLog;

import java.util.concurrent.Future;

public class Worker {
    private BaseRequest mRequest = null;
    private String mId = String.valueOf(System.nanoTime());
    private Future<?> mFuture = null;
    private ResponseListener mResponseListener = null;
    private boolean mIsCanceled = false;

    public boolean isCanceled() {
        return mIsCanceled;
    }

    public void setIsCanceled(boolean mIsCanceled) {
        this.mIsCanceled = mIsCanceled;
    }

    public Worker() {
    }

    public void setResponseListener(ResponseListener l) {
        mResponseListener = l;
    }

    public String getId() {
        return mId;
    }

    public void setFuture(Future<?> future) {
        mFuture = future;
    }

    public Future<?> getFuture() {
        return mFuture;
    }

    public void setRequest(BaseRequest req) {
        mRequest = req;
    }

    public BaseRequest getRequest() {
        return mRequest;
    }

    public void fireResponse() {
        GMLog.i("worker", "fireResponse");
        mResponseListener.onResponse(mRequest.path(), mId, mRequest.getResponseCode(), mRequest.getResponseBody());
    }
}
