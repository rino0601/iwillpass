package net.androidsea.seoultaxi.base.util;

import android.util.Log;

/**
 * 숫자 관련 계산 유틸 <br>
 */
public class CounterUtil {

    /**
     * 주어진 숫자의 자리수를 리턴함
     *
     * @param number 정수 값
     * @return 입력한 정수값의 자릿 수
     */
    public static Integer getDecimalPoint(int number) {
        for (int point = 0, divide = number; ; point++, divide /= 10) {
            if (divide < 10)
                return point;
            else
                continue;
        }
    }

    public static Integer getSimpleCount(int number) {
        int len = CounterUtil.getDecimalPoint(number);
        int firstNumber = number / (int) Math.pow(10, len - 1);
        Log.d("JH", "len:" + len);
        Log.d("JH", "firstNumber:" + firstNumber);
        return firstNumber * (int) Math.pow(10, len - 1);
    }
}
