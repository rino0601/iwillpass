package net.androidsea.seoultaxi.base.util;


import android.os.Environment;

/**
 * 외부 폴더나 파일의 경로를 관리하는 클래스로,
 * 어플리케이션에서 사용하는 모든 경로를 관리한다.<br>
 * ex) 캐시 경로 , 앱 설치 경로  , 어플리케이션 경로 등등<br>
 *
 * @author jun8126
 */
public class GMExternalPath {

    /**
     * sdcard 절대 경로
     */
    private static final String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();


    public static class FILE {

        private static final String PROFILE_IMAGE_FILENAME = "MyProfileImage.png";
    }


    /**
     * 폴더명 상수 클래스
     */
    public static class FOLDER {

        /**
         * 캐시폴더 명
         */
        private static final String CACHE = "/cache";

        /**
         * Ux log 로그 폴더명
         */
        private static final String UXLOG = "/ux_log";

        /**
         * 어플리케이션 폴더 명
         */
        private static final String APP = "/Gminute";

        /**
         * 데이터베이스 폴더 명
         */
        private static final String DB = "/database";

        private static final String PROFILE_IMAGE = "/profile_image";
    }


    public static String getProfileImageCachePath() {
        String path = getAppPath();
        if (path == null)
            return null;

        String profileImagePath = path + FOLDER.CACHE + FOLDER.PROFILE_IMAGE;
        if (StorageUtil.isExistFolder(profileImagePath) == false) {
            StorageUtil.makeFolder(profileImagePath);
        }
        return profileImagePath;
    }


    public static String getPrfilImagePath() {
        String profileImageFilePath = getProfileImageCachePath();

        if (StorageUtil.isExistFolder(profileImageFilePath) == false) {
            StorageUtil.makeFolder(profileImageFilePath);
        }
        profileImageFilePath = profileImageFilePath + "/" + FILE.PROFILE_IMAGE_FILENAME;

        return profileImageFilePath;
    }


    /**
     * <li> sdcard 절대 경로를 반환한다.
     *
     * @return 사용가능한 sdcard가 존재하지 않을 경우 null 반환
     */
    public static String getExternalSdPath() {
        if (StorageUtil.isAvailableSdcard() == false)
            return null;

        return SDCARD_PATH;
    }


    /**
     * <li> UxTrace 폴더를 반환한다.
     *
     * @return 사용가능한 sdcard가 존재하지 않을 경우 null 반환
     */
    public static String getUxLogPath() {
        String path = getAppPath();
        if (path == null)
            return null;

        String tracePath = path + FOLDER.UXLOG;
        if (StorageUtil.isExistFolder(tracePath) == false) {
            StorageUtil.makeFolder(tracePath);
        }

        return tracePath;
    }

    /**
     * 캐시 폴더의 절대경로를 반환한다.
     *
     * @return 외장sdcard가 존재하지 않을 경우 null 반환
     */
    public static String getCachePath() {
        String path = getAppPath();
        if (path == null)
            return null;

        String cachePath = path + FOLDER.CACHE;
        if (StorageUtil.isExistFolder(cachePath) == false) {
            StorageUtil.makeFolder(cachePath);
        }

        return cachePath;
    }


    /**
     * 어플리케이션의 절대 경로를 반환한다.
     *
     * @return 외장sdcard가 존재하지 않을 경우 null 반환
     */
    public static String getAppPath() {
        String path = getExternalSdPath();
        if (path == null)
            return null;

        String appPath = path + FOLDER.APP;
        if (StorageUtil.isExistFolder(appPath) == false) {
            StorageUtil.makeFolder(appPath);
        }

        return appPath;
    }


    public static String getDatabasePath() {
        String path = getAppPath();
        if (path == null)
            return null;

        String dbPath = path + FOLDER.DB;
        if (StorageUtil.isExistFolder(dbPath) == false) {
            StorageUtil.makeFolder(dbPath);
        }

        return dbPath;
    }

}
