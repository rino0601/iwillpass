package net.androidsea.seoultaxi.base.util;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Application 내에서 공용으로 사용하는 디버깅 클래스 <br><br>
 * <p/>
 * - setDebugMode(level) 을 통해 디버그 Mode를 설정 할수 있다. <br>
 * <p/>
 * - setWriteMode(level) 을 통해  파일기록Mode를 설정 할 수 있다. <br>
 */
public class GMLog {

    /**
     * Don't logcat debug or write log to file
     */
    public static final int NO_LEVEL = 0;
    /**
     * Do logcat debug or write log to file only error type
     */
    public static final int ERROR_LEVEL = 1;
    /**
     * Do logcat debug or write log to file all type
     */
    public static final int DEBUG_LEVEL = 2;


    /**
     * logcat 디버깅 모드
     */
    private static int debugMode = DEBUG_LEVEL;

    /**
     * 파일 기록  모드
     */
    private static int WriteMode = NO_LEVEL;

    private static String filenameprefix = "";

    /**
     * time log format ==> hour:minute:second
     */
    private static final int FMT_TIME = 1;
    /**
     * time log fortmat ==> year-month-day hour:minute:second
     */
    private static final int FMT_DAY_TIME = 2;

    private static SimpleDateFormat sdf_day = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat sdf_time = new SimpleDateFormat("HH:mm:ss");
    private static SimpleDateFormat sdf_dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    /**
     * logcat 디버깅 모드를 설정 한다.
     *
     * @param 'NO_LEVEL' , ERROR_LEVEL , DEBUG_LEVEL
     * @return 모드 설정의 성공 여부 반환. (잘못된 parameter를 넘길 경우 false return )
     */
    public static boolean setDebugMode(int level) {
        switch (level) {
            case NO_LEVEL:
            case ERROR_LEVEL:
            case DEBUG_LEVEL:
                debugMode = level;
                return true;
            default:
                return false;
        }
    }


    /**
     * 파일 기록 모드를 설정한다.
     *
     * @param 'NO_LEVEL' , ERROR_LEVEL , DEBUG_LEVEL
     * @return 모드 설정의 성공 여부 반환. (잘못된 parameter를 넘길 경우 false return )
     */
    public static boolean setWriteMode(int level) {
        switch (level) {
            case NO_LEVEL:
            case ERROR_LEVEL:
            case DEBUG_LEVEL: {
                WriteMode = level;
                try {
                    File folder = new File(GMExternalPath.getAppPath());
                    folder.mkdirs();
                } catch (Exception e) {
                }
                return true;
            }
            default:
                return false;
        }
    }

    public static void setWriteModeFilenamePrefix(String prefix) {
        filenameprefix = prefix;
        if (filenameprefix == null)
            filenameprefix = "";
    }


    /**
     * debugMode가 ERROR_LEVEL or DEBUG_LEVEL 일 때 logcat debug를 출력 한다.<br>
     * writeMode가 ERROR_LEVEL or DEBUG_LEVEL 일 때 파일에 로그를 출력한다.<br>
     */
    public static int e(String tag, String msg) {
        if (WriteMode != NO_LEVEL)
            writeDaily(msg, FMT_TIME);

        if (debugMode != NO_LEVEL)
            return Log.e(tag, msg);

        return 0;
    }


    /**
     * debugMode가 ERROR_LEVEL or DEBUG_LEVEL 일 때 logcat debug를 출력 한다.<br>
     * writeMode가 ERROR_LEVEL or DEBUG_LEVEL 일 때 파일에 로그를 출력한다.<br>
     *
     * @param tr 로그를 남길 Exception 객체
     */
    public static int e(String tag, String msg, Throwable tr) {
        if (WriteMode != NO_LEVEL)
            writeErrorLog(msg, tr);

        if (debugMode != NO_LEVEL)
            return Log.e(tag, msg, tr);

        return 0;
    }

    public static void d(String tag, String text) {
        if (WriteMode != NO_LEVEL)
            writeDaily(text, FMT_TIME);

        if (debugMode != NO_LEVEL)
            Log.d(tag, text);
    }

    public static void w(String tag, String text) {
        if (WriteMode != NO_LEVEL)
            writeDaily(text, FMT_TIME);

        if (debugMode != NO_LEVEL)
            Log.w(tag, text);
    }


    /**
     * debugMode가 DEBUG_LEVEL 일 때만 logcat debug를 출력 한다.<br>
     * writeMode가 DEBUG_LEVEL 일 때만 파일에 로그를 출력한다.<br>
     */
    public static int i(String tag, String msg) {
        if (WriteMode == DEBUG_LEVEL)
            writeDaily(msg, FMT_TIME);

        if (debugMode == DEBUG_LEVEL)
            if (debugMode != ERROR_LEVEL)
                return Log.i(tag, msg);

        return 0;
    }


    /**
     * 파일에 Log를 출력한다.
     *
     * @param fname 파일명
     * @param msg   기록할  Log 메시지
     * @param op    기록할 시간 format (FMT_TIME , FMT_DAY_TIME )
     */
    private static void write(String fname, String msg, int op) {
        BufferedWriter out = null;

        try {
            File file = new File(fname);

            if (file.exists() && !file.canWrite()) {
                return;
            }

            out = new BufferedWriter(new FileWriter(file, true));

            switch (op) {
                case FMT_TIME:
                    out.write(sdf_time.format(new java.util.Date()) + "\t" + msg + "\r");
                    break;
                case FMT_DAY_TIME:
                    out.write(sdf_dayTime.format(new java.util.Date()) + "\t" + msg + "\r");
                    break;
                default:
                    out.write(msg);
                    break;
            }

            // BufferedWriter의 특징이다. 개행 \n은 각 시스템 별로 다를 수 있음으로 newLine함수로 개행을 시켜준다.
            out.newLine();
            out.flush();
        } catch (Exception e) {
            Log.e("log", e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                    out = null;
                }
            } catch (IOException t) {
                t.printStackTrace();
            }
        }
    }


    private static void writeDaily(String msg, int op) {
        if (msg == null)
            return;
        write(GMExternalPath.getAppPath() + "/" + filenameprefix + sdf_day.format(new java.util.Date()) + ".txt", msg, op);
    }


    /**
     * 에러 메시지를 파일에 출력한다.
     *
     * @param msg   출력할 에러  메시지.
     * @param error Exception object
     */
    public static void writeErrorLog(String msg, Throwable error) {
        StringBuffer errorStrBuf = null;
        StackTraceElement[] stackTraceElement = null;

        if (msg == null)
            return;

        if (error != null) {
            stackTraceElement = error.getStackTrace();

            if (stackTraceElement != null) {
                errorStrBuf = new StringBuffer(msg + "\r\n\r\n" + error.toString() + "\r\n");

                for (int i = 0; i < stackTraceElement.length; i++) {
                    if (i == stackTraceElement.length - 1) {
                        if (stackTraceElement[i].getFileName() == null) {
                            errorStrBuf.append("          at " + stackTraceElement[i].getClassName() + "." + stackTraceElement[i].getMethodName());
                        } else {
                            errorStrBuf.append("          at " + stackTraceElement[i].getClassName() + "." + stackTraceElement[i].getMethodName() + " (" + stackTraceElement[i].getFileName() + ":" + stackTraceElement[i].getLineNumber() + ")");
                        }
                    } else {
                        if (stackTraceElement[i].getFileName() == null) {
                            errorStrBuf.append("          at " + stackTraceElement[i].getClassName() + "." + stackTraceElement[i].getMethodName() + "\r\n");
                        } else {
                            errorStrBuf.append("          at " + stackTraceElement[i].getClassName() + "." + stackTraceElement[i].getMethodName() + " (" + stackTraceElement[i].getFileName() + ":" + stackTraceElement[i].getLineNumber() + ")\r\n");
                        }
                    }
                }
            } else {
                errorStrBuf = new StringBuffer(msg);
            }
        } else {
            errorStrBuf = new StringBuffer(msg);
        }

        writeDaily(errorStrBuf.toString(), FMT_DAY_TIME);
    }

    // [sinaeyo][2011.09.09] 타임 로그 찍는 함수
    public static long logTime = 0;

    public static void writeTimeLog(String message) {
        long current = Calendar.getInstance().getTimeInMillis();
        GMLog.e("time", message + " (" + (current - GMLog.logTime) + ")");
        GMLog.logTime = current;
    }
}
