package net.androidsea.seoultaxi.base.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Environment;
import android.os.StatFs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 외부 파일과의  쓰기 , 읽기 작업을  도와주는 util 클래스 <br>
 * 비트맵 이미지   저장 , 읽기 <br>
 */
public class StorageUtil {

    private static final String TAG = StorageUtil.class.getName();

    public static long remainExternalStorage() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (long) stat.getFreeBlocks() * (long) stat.getBlockSize();
    }

    /**
     * 지정된 경로의 폴더를 생성하고 결과를 반환한다.
     *
     * @param path 생성할 폴더의 절대경로
     * @return 이미 폴더가 존재하거나 생성 실패시 false 반환 , 만들었을 경우 true 반환
     */
    public static boolean makeFolder(String path) {
        File file = new File(path);

        if (file.exists() && file.isDirectory())
            return false;

        return file.mkdirs();
    }


    /**
     * 지정한 절대경로에 폴더가 존재여부를 체크한다
     *
     * @param path 폴더의 절대경로
     * @return 존재하면 true , 없으면 false 반환
     */
    public static boolean isExistFolder(String path) {
        File file = new File(path);

        return file.exists();
    }


    /**
     * 파일에 저장
     *
     * @param path       저장할 경로
     * @param content    저장할 내용
     * @param appendable 이어쓰기 옵션  true면 이어쓰고 fale면 덮어쓴다.
     * @return 저장 성공 여부
     */
    public static boolean writeFile(String path, String content, boolean appendable) {
        BufferedWriter out = null;

        try {
            File file = new File(path);
            if (file.exists() && !file.canWrite()) {
                return false;
            }

            out = new BufferedWriter(new FileWriter(file, appendable));
            out.write(content);
            out.flush();

        } catch (Exception e) {
            GMLog.e(TAG, e.getMessage(), e);
            return false;
        } finally {
            try {
                if (out != null) {
                    out.close();
                    out = null;
                }
            } catch (IOException t) {
                GMLog.e(TAG, t.getMessage(), t);
                return false;
            }
        }
        return true;
    }


    /**
     * 로컬 파일을 삭제한다. <br>
     * 디렉토리 일경우 하위 파일도 삭제한다.
     *
     * @param filepath 삭제할 파일 path
     * @return
     */
    public static boolean deleteFile(String filepath) {
        if (filepath == null)
            return false;

        File file = new File(filepath);

        if (file.exists() == false)
            return false;

        //File f = null;
        if (file.isDirectory() == true) {
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory() == true) {
                        StorageUtil.deleteFile(files[i].getAbsolutePath());
                    }
                    files[i].delete();
                }
            }
        }

        return file.delete();
    }


    /**
     * <li>sd카드가 사용가능한 상태인지를 확인한다.
     * <li>내장sdcard,외장 sdcard 둘중 하나만 사용가능해도 true를 반환
     *
     * @return <li>사용가능하면 true
     * <li>사용 불가능하면 false
     */
    public static boolean isAvailableSdcard() {
        return Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED);
    }


    /**
     * 현재 사용가능한 sd카드 메모리 사이즈를 반환한다<br>
     *
     * @return <li>내장 sd카드가 존재할경우: (외장sd유무에 상관없이) 내장 sdcard 크기를 반환
     * <li>외장 sd카드만 존재할경우: 외장 sdcard 크기를 반환
     * <li>사용가능한 sd카드가 없는경우 -1반환
     */
    public static long getAvailableExternalMemorySize() {
        if (isAvailableSdcard()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return availableBlocks * blockSize;
        } else {
            return -1;
        }
    }

    /**
     * 현재 total sdcard 메모리 사이즈를 반환한다<br>
     *
     * @return <li>내장 sd카드가 존재할경우:(외장sd유무에 상관없이) 내장 sdcard 크기를 반환
     * <li>외장 sd카드만 존재할경우: 외장 sdcard 크기를 반환
     * <li>사용가능한 sd카드가 없는경우 -1반환
     */
    public static long getTotalExternalMemorySize() {
        if (isAvailableSdcard()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return totalBlocks * blockSize;
        } else {
            return -1;
        }
    }


    /**
     * formatting된  사용가능한 sd카드 메모리 사이즈를 반환한다<br>
     * <li> 크기에 따라  KB , MB 단위로  반환
     *
     * @return <li>내장 sd카드가 존재할경우: (외장sd유무에 상관없이) 내장 sdcard 크기를 반환
     * <li>외장 sd카드만 존재할경우: 외장 sdcard 크기를 반환
     * <li>사용가능한 sd카드가 없는경우 null
     */
    public static String getFormattedAvailableExternalMemorySize() {
        long size = getAvailableExternalMemorySize();
        if (size < 0)
            return null;

        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null)
            resultBuffer.append(suffix);
        return resultBuffer.toString();
    }


    // asset 에 있는 리소스를 지정한 경로에 복사, 이미 해당경로에 동일 이름의 파일이 존재하면 바로 성공을 리턴
    // src : asset 의 상대경로
    // des : copy 할 대상경로
    // return 성공:true, 실패:false
    public static boolean copyFromAsset(Context context, String src, String des) {
        if (context == null || src == null || des == null)
            return false;

        File bi = new File(des);
        if (bi.exists() == false) {
            AssetManager assetMgr = context.getAssets();
            InputStream is = null;
            FileOutputStream out = null;
            try {
                is = assetMgr.open(src, AssetManager.ACCESS_BUFFER);
                byte[] buffer = new byte[1024];
                int length = 0;

                out = new FileOutputStream(des);
                while ((length = is.read(buffer)) != -1) {
                    out.write(buffer, 0, length);
                }
                is.close();
                out.close();

            } catch (IOException e) {
                try {
                    is.close();
                    out.close();
                } catch (IOException e1) { //ignore exception
                    e1.printStackTrace();
                    return false;
                }
                e.printStackTrace();
                return false;
            }

            return true;
        } else {
            return true;
        }
    }


    /**
     * 지정된 fpath에 bitmap 파일을 저장한다
     *
     * @param fpath  bitmap을 저장할 파일 경로
     * @param bitmap 저장할 Bitmap 객체
     * @return 성공 :true  , 실패 : false
     * @throws java.io.IOException
     */
    public static boolean saveBitmap(String fpath, Bitmap bitmap) throws IOException {
        if (fpath == null || bitmap == null) {
            return false;
        }

        GMLog.e(TAG, "saveBitmap() path " + fpath + " bitmap size " + bitmap.getWidth() * bitmap.getHeight() * 4);

        OutputStream out = null;
        boolean isSuccess = false;

        try {
            File saveFile = new File(fpath);
            //동일한 경로에 file이 존재하더라도 덮어쓴다. 이미 존재할경우 createNewFile()의 리턴값은 false
            saveFile.createNewFile();
            out = new FileOutputStream(saveFile);

            isSuccess = bitmap.compress(CompressFormat.PNG, 100, out);

//			isSuccess = true;
        } catch (Exception e) {
            if (out != null) {
                out.close();
            }
            GMLog.e(TAG, e.getMessage(), e);
            isSuccess = false;
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                    out = null;
                }
            } catch (IOException e) {
                GMLog.e(TAG, e.getMessage(), e);
            }
        }

        return isSuccess;
    }


}
